# CelestialPy

CelestialPy is a Python packages containing tools to solve problems in celestial mechanics (space flight, space debris). This version is an open version of the package used to handle a resident space object database at the IMCCE.

For any problem, remark, contribution. Send a mail at alexis.petit@obspm.fr

### Prerequisites

Before the installing of the Python package, you need to install Python 2.7, and several Python packages. For this purpose you can use miniconda

```
conda create -n celestialpy_lite python=2.7
conda install -n celestialpy_lite pip numpy scipy pandas jdcal

```

But a script is avaiable to perform directly the installation.

```
sh install.sh
```

You can open the Python environment.


```
source activate celestialpy_lite
```

And you close it.

```
source activate celestialpy_lite
```

### Installing

You can excecute the scripts going in the *bin* directory, but you can also install the Python package and to excecute the scripts in any place on your desktop

```
python setup.py install
```

## Applications

Several algorithms have been implemented following the book of D. Vallado (2013). Then, to check the results execute the following script located in the *bin* directory

```
python vallado.py
```

Moreover, three applications are proposed.

#### Orbit Propagation

CelestialPy contains a wrapper to use several orbit propagators. For example you can download and compile the NIMASTEP/SYMPLEC software.

```
https://gitlab.obspm.fr/apetit/nimastep-symplec.git
```

In order to propagate an orbit with CelestialPy, you need a configuration file given in a *data* directory. You can specify the parameters of the propagation, the force model. You can use it with a script given in the *bin* directory and the following options

```
python propagation.py -p --param ../data/nimastep.cfg
```

#### NASA Breakup Model

CelestialPy contains a wrapper to use the NASA Breakup Model (NBM) available there

```
git clone https://gitlab.obspm.fr/apetit/nasa-breakup-model.git
```

It allows to create a cloud of space debris. A script using the package allows to call the NBM a plot the characteristics of the cloud of space debris created. 

```
python breakup.py -b -f
```

You can propagate this cloud and plot snapshots adding an optional argument.

```
python breakup.py -b -f -p
```

#### Space debris model

A script call the space debris model proposed in the CelestialPy package.

```
python call_sdm.py -s
```

## Authors

* **Alexis Petit** - *Initial work* - [apetit](https://gitlab.obspm.fr/apetit)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

This project was developed during my scientific career in different laboratories.

* Department of mathematics, UNAMUR, Namur, Belgium.
* IMCCE, Observatory of Paris, Paris, France.
* IFAC-CNR, Sesto Forientino, Italy.

