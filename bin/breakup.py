#!/usr/bin/python
# Licensed under a 3-clause BSD style license - see LICENSE.rst
# -*-coding:Utf-8 -*

"""
  breakup.py
  Author: Alexis Petit, PhD, IMCCE, Observatoire de Paris
  Date: 2018 02 07
  This script is a part of CelestialPy. It allows to compute
  the set of initial conditions of a fragments of a cloud of
  space debris using the NASA Breakup Model.
"""


# Packages


import argparse
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pandas as pd
import numpy as np
import time
import sys
import os
from datetime import datetime,timedelta


# Packages CelestialPy


sys.path.insert(0,os.path.abspath(os.path.join(os.path.dirname(__file__),'..')))

from celestialpy_lite import earth_eq_radius,earth_mu 
from celestialpy_lite import write_schedule,read_param_nbm,write_param_file
from celestialpy_lite import call_nbm
from celestialpy_lite import propagate
from celestialpy_lite import convert_ephemeris_2_snapshot
from celestialpy_lite import df_kepl_2_cart


# Code


def write_breakup_file():
  """ Write an example of breakup event.
  """  

  name = 'TEST'
  idsat = 1
  date = datetime(2016,1,1,0,0,0)
  type_frag = 'explosion'
  scale_factor = 1.
  sma   = 7400.E3 #m
  ecc   = 0.001  
  inc   = 20.     #degrees
  raan  = 100.    #degrees
  omega = 100.    #degrees
  ma    = 10.     #degrees
  mass  = 100     #kg
  
  data = []
  data.append([name,idsat,date,type_frag,scale_factor,sma,ecc,inc,raan,omega,ma,mass])
  headers = ['Name','Id Norad','Date','Type','S','a[m]','e','i[deg]','RAAN[deg]','Omega[deg]','MA[deg]','Mass[kg]']
  data = pd.DataFrame(data,columns=headers)
  print('Breakup event:')
  print(data)
  data.to_csv('breakup_event.csv',index=False)

  
def create_space_debris_cloud(work_dir,scheduler_file,param_file,propagation=False,verbosity=True):
  """ Create a cloud of space debris for a breakup event given.

  INPUTS
  ------

  work_dir: string 
    Working directory.
  local_db: object
    Object giving data about the local database.
  scheduler_file: string
    Name of the file containing data about the parent body.
  param_file: string
    Name of the file containing the parameters of the NBM.

  OPTIONAL INPUTS
  ---------------  

  propagation: logical
    Propagate the cloud of space debris.
  verbosity: logical
    Show informations.

  """

  nbm_path = '/home/alexis/ODIN/softwares/nasa-breakup-model'
  
  minimal_size = 0.1 #meters

  # Read breakup event, you can create it with: python breakup.py -w

  event               = pd.read_csv(scheduler_file)
  event['Date']       = pd.to_datetime(event['Date'])
  type_frag           = event['Type'].iloc[0]
  id_norad            = event['Id Norad'].iloc[0]  
  schedule_file       = write_schedule(work_dir,event)
  param_nbm           = read_param_nbm(param_file,type_frag)
  param_nbm['limits']['size_min'] = minimal_size
  param_file          = write_param_file(work_dir,type_frag,id_norad,param_nbm)
  fragments,dv        = call_nbm(nbm_path,schedule_file,param_file,output=work_dir+'/breakup',clean=True,verbosity=True)
  fragments.to_csv('{0}/ic_cloud_{1}.csv'.format(work_dir,id_norad),index=False)

  selected_headers = ['Julian_Day','a[m]','e','i[deg]','RAAN[deg]','Omega[deg]','MA[deg]','BC[m2/kg]','Mass[kg]','Size[m]']
  fragments[selected_headers] = fragments[selected_headers].astype(float)
  fragments['apogee']         = fragments['a[m]']*(1+fragments['e']) 
  fragments['perigee']        = fragments['a[m]']*(1-fragments['e'])
  fragments['period']         = 2*np.pi*np.sqrt(fragments['a[m]']**3/earth_mu)/3600.

  cd = 2.2
  bc_frag_list = fragments['BC[m2/kg]'].values
  log_am_frag  = [np.log10(float(item)/cd) for item in bc_frag_list] 
  mass_frag_list = fragments['Mass[kg]'].values
  log_mass_frag  = [np.log10(float(item)) for item in mass_frag_list]
  size_frag_list = fragments['Size[m]'].values
  log_size_frag  = [np.log10(float(item)) for item in size_frag_list]
  total_mass = sum(mass_frag_list)

  if os.path.isdir(work_dir+'/figures'):
    os.system('rm -r '+work_dir+'/figures')
  os.system('mkdir '+work_dir+'/figures')

  fig = plt.figure(figsize=(9,7))
  ax1 = fig.add_subplot(1,1,1)
  ax1.spines["top"].set_visible(False)
  ax1.spines["right"].set_visible(False)
  ax1.set_xlabel('Log10(Size[m])',fontsize=14)
  ax1.set_ylabel('Number of debris',fontsize=14)  
  plt.hist(log_size_frag,bins=100,label='NBM ('+str(len(bc_frag_list))+')')
  plt.savefig(work_dir+'/figures/size')
  plt.close()

  fig = plt.figure(figsize=(9,7))
  fig.suptitle('Total mass of the fragments: {0:.2f} kg'.format(total_mass))
  ax1 = fig.add_subplot(1,1,1)
  ax1.spines["top"].set_visible(False)
  ax1.spines["right"].set_visible(False)
  ax1.set_xlabel('Log10(Mass[kg])',fontsize=14)
  ax1.set_ylabel('Number of debris',fontsize=14)  
  plt.hist(log_mass_frag,bins=100,label='NBM ('+str(len(bc_frag_list))+')')
  plt.savefig(work_dir+'/figures/mass')
  plt.close()
  
  fig = plt.figure(figsize=(9,7))
  ax1 = fig.add_subplot(1,1,1)
  ax1.spines["top"].set_visible(False)
  ax1.spines["right"].set_visible(False)
  ax1.set_xlabel('Log10(A/M)',fontsize=14)
  ax1.set_ylabel('Number of debris',fontsize=14)  
  plt.hist(log_am_frag,bins=100,label='NBM ('+str(len(bc_frag_list))+')')
  plt.savefig(work_dir+'/figures/am')
  plt.close()
  
  fig, ax1= plt.subplots(figsize=(8,6))
  ax1.spines["top"].set_visible(False)
  ax1.spines["right"].set_visible(False)
  ax1.set_ylabel('Apogee/Perigee - Re [km]',fontsize=14)
  ax1.set_xlabel('Period [hour]',fontsize=14)
  ax1.plot(fragments['period'],(fragments['apogee']-earth_eq_radius)/1000.,'o',ms=3,c='b',alpha=0.2,label='Apogee')
  ax1.plot(fragments['period'],(fragments['perigee']-earth_eq_radius)/1000.,'o',ms=3,c='r',alpha=0.2,label='Perigee')
  plt.legend(loc='upper left')
  plt.savefig(work_dir+'/figures/gabbard')
  plt.close()
  
  if propagation:

    fragments['Id Norad'] = 1
    for i in range(0,len(fragments),1):
      fragments['Id Norad'].iloc[i] = i+1
    fragments['Date'] = pd.to_datetime(fragments['Date'])
      
    parameters = {}
    parameters['orbit_propagator'] = '../data/breakup/op_breakup/nimastep_J2_SM_drag.cfg'
    parameters['time_span']        = 1 #days
    parameters['output_step']      = 3600 #seconds
    cloud_propagation(work_dir,fragments,parameters)
  
    
def cloud_propagation(work_dir,population,parameters):
  """ We propagate each fragments of the cloud of space debris.

  INPUTS
  ------

  work_dir: string
    Working directory.
  fragments: dataframe
    Initial conditions of the cloud of space debris.
  parameters: dictionary
    Parameters of the propagation.

  """

  os.system('sed -i -e "s/.*output_step_day.*/output_step_day = {0}/g" {1}'.format(0,parameters['orbit_propagator']))
  os.system('sed -i -e "s/.*output_step_sec.*/output_step_sec = {0}/g" {1}'.format(parameters['output_step'],parameters['orbit_propagator']))
  os.system('sed -i -e "s/.*time_day.*/time_day = {0}/g" {1}'.format(parameters['time_span'],parameters['orbit_propagator'])) 
  list_ephemeris = propagate(work_dir,population,parameters['orbit_propagator'])
  snapshots = convert_ephemeris_2_snapshot(list_ephemeris)

  pop_dir = work_dir+'/cloud_evolution'
  if os.path.isdir(pop_dir):
    os.system('rm -r '+pop_dir)
  os.system('mkdir '+pop_dir)   

  for snapshot in snapshots:

    date = snapshot[0]
    name = 'population_'+date.strftime('%Y%m%dT%H%M%S')
    snapshot = df_kepl_2_cart(snapshot[1])    

    fig = plt.figure()
    fig.suptitle(date)    
    ax = fig.add_subplot(111,projection='3d')
    dist = 1.5E6+earth_eq_radius
    ax.set_xlim(-dist,+dist)
    ax.set_ylim(-dist,+dist)
    ax.set_zlim(-dist,+dist)    
    ax.plot3D(snapshot['x[m]'],snapshot['y[m]'],snapshot['z[m]'],'ro',markersize=1)
    plt.savefig(pop_dir+'/'+name)
    plt.close()
    
  
##########################################################################

##########################################################################

###########################################################################
#
# Main
if __name__ == "__main__":
 
  parser = argparse.ArgumentParser(description='NASA Breakup Model')
  parser.add_argument("-w","--write",help="write parameters of a breakup event",action="store_true")
  parser.add_argument("-b","--nasa_breakup_model",help="call the NASA Breakup Model to create a cloud of space debris",action="store_true")
  
  # Options

  parser.add_argument("--parentbody",help="information about the parent body",type=str)
  parser.add_argument("--parameters",help="parameters of the NASA Breakup Model",type=str)
  
  parser.add_argument("-p","--propagation",help="propagate the cloud of space debris",action="store_true")  
  parser.add_argument("-f","--folder",help="save data in a folder",action="store_true")
  parser.add_argument("-v","--verbosity",help="print informations about all the process",action="store_true")
  
  args = parser.parse_args()

  if args.folder:
    working_dir = True
  else:
    working_dir = False
    
  if working_dir==True:
    date = datetime.today()
    work_dir = 'temp_'+date.strftime('%H%M%S')
    if os.path.isdir(work_dir):
      os.system('rm -r '+work_dir)
    os.system('mkdir '+work_dir)
    work_dir = work_dir
  else:
    work_dir = '.'

  if args.write:
    write_breakup_file()
    
  if args.nasa_breakup_model:
    if (args.parentbody and args.parameters):
      create_space_debris_cloud(work_dir,args.parentbody,args.parameters,propagation=args.propagation,verbosity=args.verbosity)
    else:
      print('Arguments are missing (information about the parent body and parameters of the NASA Breakup Model)')
