#!/usr/bin/python
# Licensed under a 3-clause BSD style license - see LICENSE.rst
# -*-coding:Utf-8 -*

"""
  breakup.py
  Author : Alexis Petit, PhD, IMCCE, Observatoire de Paris
  Date : 2018 02 07
  This script is a part of CelestialPy. It allows to compute
  the set of initial conditions of a fragments of a cloud of
  space debris using the NASA Breakup Model.
"""


# Packages


import argparse
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pandas as pd
import numpy as np
import time
import sys
import os
from datetime import datetime,timedelta


# Packages CelestialPy


sys.path.insert(0,os.path.abspath(os.path.join(os.path.dirname(__file__),'..')))

from celestialpy_lite import earth_eq_radius,earth_mu 
from celestialpy_lite import wrapper_nbm
from celestialpy_lite import propagate
from celestialpy_lite import convert_ephemeris_2_snapshot
from celestialpy_lite import df_kepl_2_cart


# Code
  
def create_space_debris_cloud(work_dir,verbosity=True):
  """ Create a cloud of space debris for a breakup event given.

  INPUTS
  ------

  work_dir: string 
    Working directory.

  OPTIONAL INPUTS
  ---------------  

  verbosity: logical
    Show informations.

  """

  minimal_size = 0.1 #meters
  nbm_path = '/Users/alexis/Documents/ODIN/softwares/nasa-breakup-model'
  scheduler_file = '../data/breakup_event.csv'
  breakup = pd.read_csv(scheduler_file)
  fragments,dv = wrapper_nbm(work_dir,nbm_path,breakup,minimal_size,add_dv=True,clean=True)

  selected_headers = ['Julian_Day','a[m]','e','i[deg]','RAAN[deg]','Omega[deg]','MA[deg]','BC[m2/kg]','Mass[kg]','Size[m]']
  fragments[selected_headers] = fragments[selected_headers].astype(float)

  fragments['Id Norad'] = 1
  for i in range(0,len(fragments),1):
    fragments['Id Norad'].iloc[i] = i+1
  fragments['Date'] = pd.to_datetime(fragments['Date'])
      
  parameters = {}
  parameters['orbit_propagator'] = '../data/nimastep.cfg'
  parameters['path'] = '/Users/alexis/Documents/ODIN/softwares/nimastep-symplec'
  cloud_propagation(work_dir,fragments,parameters)

    
def cloud_propagation(work_dir,population,parameters):
  """ We propagate each fragments of the cloud of space debris.

  INPUTS
  ------

  work_dir: string
    Working directory.
  fragments: dataframe
    Initial conditions of the cloud of space debris.
  parameters: dictionary
    Parameters of the propagation.

  """
  
  list_ephemeris = propagate(work_dir,parameters['path'],population,parameters['orbit_propagator'])
  snapshots = convert_ephemeris_2_snapshot(list_ephemeris)

  pop_dir = work_dir+'/cloud_evolution'
  if os.path.isdir(pop_dir):
    os.system('rm -r '+pop_dir)
  os.system('mkdir '+pop_dir)   

  torus_dir = work_dir+'/torus'
  if os.path.isdir(torus_dir):
    os.system('rm -r '+torus_dsir)
  os.system('mkdir '+torus_dir) 
  
  for snapshot in snapshots:

    date = snapshot[0]
    name = 'population_'+date.strftime('%Y%m%d')
    snapshot = df_kepl_2_cart(snapshot[1])    

    fig = plt.figure()
    fig.suptitle(date)    
    ax = fig.add_subplot(111,projection='3d')
    dist = 1.5E6+earth_eq_radius
    ax.set_xlim(-dist,+dist)
    ax.set_ylim(-dist,+dist)
    ax.set_zlim(-dist,+dist)    
    ax.plot3D(snapshot['x[m]'],snapshot['y[m]'],snapshot['z[m]'],'ro',markersize=1)
    plt.savefig(pop_dir+'/'+name)
    plt.close()

    fig = plt.figure()
    fig.suptitle(date)    
    ax = fig.add_subplot(111,projection='3d')
    plt.savefig(torus_dir+'/'+name)
    plt.close()    
 
    
##########################################################################

##########################################################################

###########################################################################
#
# Main
if __name__ == "__main__":


  working_dir = True    
  if working_dir==True:
    date = datetime.today()
    work_dir = 'temp_'+date.strftime('%H%M%S')
    if os.path.isdir(work_dir):
      os.system('rm -r '+work_dir)
    os.system('mkdir '+work_dir)
    work_dir = work_dir
  else:
    work_dir = '.'

  create_space_debris_cloud(work_dir)
