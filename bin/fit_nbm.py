#!/usr/bin/python
# Licensed under a 3-clause BSD style license - see LICENSE.rst
# -*-coding:Utf-8 -*

"""
  fit_nbm.py
  Author : Alexis Petit, PhD, IMCCE, Observatoire de Paris
  Date: 2019 08 29
  This script is a part of CelestialPy. It allows to fit the 
  parameters of the NASA Breakup Model (NBM) using the TLE data.

  Three modes:
    - Comparison
    - Fitting
    - Retropropagation
"""


# Packages


import argparse
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pandas as pd
import numpy as np
import random
import time
import sys
import os
import copy
from datetime import datetime,timedelta


# Packages CelestialPy


sys.path.insert(0,os.path.abspath(os.path.join(os.path.dirname(__file__),'..')))

from celestialpy_lite import earth_eq_radius,earth_mu 
from celestialpy_lite import propagate
from celestialpy_lite import convert_ephemeris_2_snapshot
from celestialpy_lite import df_kepl_2_cart
from celestialpy_lite import init_param_nbm,write_param_file,write_schedule
from celestialpy_lite import call_nbm


# Code


nbm_path = '/Users/alexis/Documents/ODIN/softwares/nasa-breakup-model'
#path_op = '/Users/alexis/Documents/ODIN/softwares/nimastep-symplec'
path_op = '/Users/alexis/Documents/ODIN/softwares/orbit-propagation-imcce'

  
def breakup_comparison(work_dir,tle_population,scheduler_file,param_file,verbosity=True):
  """ Comparison between the NASA Breakup Model and TLE data.

  INPUTS
  ------

  work_dir: string 
    Working directory.
  tle_population: dataframe
    TLE population.
  scheduler_file: string
    File name of the breakup event.
  param_file: string
    Parameters of the NBM.

  OPTIONAL INPUTS
  ---------------  

  verbosity: logical
    Show informations.

  """
  
  # Read parameters
  population = pd.read_csv(tle_population)
  population['Date'] = pd.to_datetime(population['Date'])
  event = pd.read_csv(scheduler_file)
  event['Date'] = pd.to_datetime(event['Date'])  
  type_frag = event['Type'].iloc[0]
  id_norad = event['Id Norad'].iloc[0]
  date_i = event['Date'].iloc[0]
  date_f = population['Date'].iloc[0]  
  
  # Compute the cloud of fragments
  schedule_file       = write_schedule(work_dir,event)
  fragments           = call_nbm(nbm_path,schedule_file,param_file,output=work_dir+'/breakup',clean=True,verbosity=True)[0]
  final_fragments     = cloud_propagation(work_dir,date_i,date_f,fragments)
  di                  = distance_apo(work_dir,population,final_fragments,plot_distri=True,plot_gabbard=True,name_file='apogee_distribution')  

  
def nbm_fitting(work_dir,tle_population,scheduler_file,verbosity=True):
  """ Fit the NASA Breakup Model.

  INPUTS
  ------

  work_dir: string 
    Working directory.
  tle_population: dataframe
    TLE population.
  scheduler_file: string
    File name of the breakup event.

  OPTIONAL INPUTS
  ---------------  

  verbosity: logical
    Show informations.

  """
  
  # Initialisation

  res_dir = work_dir+'/results'
  os.system('mkdir '+res_dir)
  
  algo_param = {}
  algo_param['t'] = 1.
  algo_param['t_min'] = 0.001
  algo_param['alpha'] = 0.95
  algo_param['max_it'] = 100.
  algo_param['accuracy'] = 10.
  
  minimal_size = 0.1 #meters    
  population = pd.read_csv(tle_population)
  population['Date'] = pd.to_datetime(population['Date'])
  event = pd.read_csv(scheduler_file)
  event['Date'] = pd.to_datetime(event['Date'])
  date_i = event['Date'].iloc[0]
  date_f = population['Date'].iloc[0]
  type_frag = event['Type'].iloc[0]
  id_norad = event['Id Norad'].iloc[0]
  param_nbm = init_param_nbm(type_frag)
  param_nbm['d_dv']['coeff_vel'] = 1.0
  param_nbm['limits']['coeff_am'] = 0.0
  param_nbm['limits']['size_min'] = minimal_size
  event['S'] = 1.5

  # Initial distribution

  step = 0
  
  param_file          = write_param_file(work_dir,type_frag,id_norad,param_nbm)
  schedule_file       = write_schedule(work_dir,event)
  fragments           = call_nbm(nbm_path,schedule_file,param_file,output=work_dir+'/breakup',clean=False,verbosity=True)[0]
  final_fragments     = cloud_propagation(work_dir,date_i,date_f,fragments)
  di                  = distance_apo(work_dir,population,final_fragments,plot_distri=True,name_file='apogee_'+str(step))
  print('Step {0} | Distance {1}'.format(step,di))

  list_step = []
  list_dist = []
  list_step.append(step)  
  list_dist.append(di)
  
  # Modified distribution
  
  new_event = copy.deepcopy(event)
  new_event['S'] = new_event['S']+random.uniform(0,1)*2-1.
  if new_event['S'].iloc[0]<=0:
    new_event['S'] = 0.1
  new_param_nbm = copy.deepcopy(param_nbm)
  new_param_nbm['d_dv']['coeff_vel'] += random.uniform(0,1)*0.2-0.1 

  step += 1
  
  param_file          = write_param_file(work_dir,type_frag,id_norad,new_param_nbm)
  schedule_file       = write_schedule(work_dir,new_event)
  new_fragments       = call_nbm(nbm_path,schedule_file,param_file,output=work_dir+'/breakup',clean=False,verbosity=True)[0]
  new_final_fragments = cloud_propagation(work_dir,date_i,date_f,new_fragments)  
  new_di              = distance_apo(work_dir,population,new_final_fragments,plot_distri=True,name_file='apogee_'+str(step))
  
  while (step<algo_param['max_it'] and di>algo_param['accuracy']):

    # Temperature condition -> Find global minimum
    cdt_temp = (new_di>di and (np.exp((di-new_di)/algo_param['t'])>random.uniform(0,1)))

    # Acceptance Probability
    if (new_di<di or cdt_temp):

      # Save
      event = copy.deepcopy(new_event)
      param_nbm = copy.deepcopy(new_param_nbm)
      di = new_di

      event.to_csv(res_dir+'/event.csv',index=False)
      param_file = write_param_file(res_dir,type_frag,id_norad,param_nbm)
      
      list_step.append(step)  
      list_dist.append(di)      

      step += 1
      
    print('Step {0} | Distance {1}'.format(step,di))

    # Modified distribution

    new_event = copy.deepcopy(event)
    new_event['S'] = new_event['S']+random.uniform(0,1)*1-0.5
    if new_event['S'].iloc[0]<0:
      new_event['S'] = 0.1
    new_param_nbm = copy.deepcopy(param_nbm)
    new_param_nbm['d_dv']['coeff_vel'] += random.uniform(0,1)*0.2-0.1 
       
    param_file          = write_param_file(work_dir,type_frag,id_norad,new_param_nbm)
    schedule_file       = write_schedule(work_dir,new_event)
    new_fragments       = call_nbm(nbm_path,schedule_file,param_file,output=work_dir+'/breakup',clean=False,verbosity=True)[0]
    new_final_fragments = cloud_propagation(work_dir,date_i,date_f,new_fragments)  
    new_di              = distance_apo(work_dir,population,new_final_fragments,plot_distri=True,name_file='apogee_'+str(step))     
    
    # Update t
    if (algo_param['t']<algo_param['t_min']):
      break
    else:
      algo_param['t'] = algo_param['alpha']*algo_param['t']

  # Plot convergence

  fig, ax1= plt.subplots(figsize=(8,6))
  ax1.spines["top"].set_visible(False)
  ax1.spines["right"].set_visible(False)
  ax1.set_ylabel('Distance',fontsize=14)
  ax1.set_xlabel('Step',fontsize=14)
  ax1.plot(list_step,list_dist)
  plt.savefig(work_dir+'/convergence')
  plt.close()  
      
  # Save NBM parameters

  event.to_csv(work_dir+'/final_breakup_event.csv',index=False)


def cloud_propagation(work_dir,date_i,date_f,fragments):

  dt = date_f-date_i
  days = dt.days
  
  print('Propagation from {0} to {1} let {2} days'.format(date_i,date_f,days))
  fragments['Id Norad'] = 1
  for i in range(1,len(fragments),1):
    fragments['Id Norad'].iloc[i] = i
  #op_param = '../data/breakup/nimastep_J2_SM_drag.cfg'
  op_param = '../data/breakups/satlight_J2_SM_DTM2013.cfg'
  output_step_time = 3600*24*1
  os.system('sed -i -e "s/.*output_step_day.*/output_step_day = {0}/g" {1}'.format(0,op_param))
  os.system('sed -i -e "s/.*output_step_sec.*/output_step_sec = {0}/g" {1}'.format(output_step_time,op_param))
  os.system('sed -i -e "s/.*time_day.*/time_day = {0}/g" {1}'.format(days,op_param)) 
  list_ephemeris = propagate(work_dir,path_op,fragments,op_param)  
  snapshots = convert_ephemeris_2_snapshot(list_ephemeris)
  #plot_set_ephemeris(work_dir,list_ephemeris)
  #plot_set_snapshots(work_dir,snapshots)
  final_fragments = copy.deepcopy(snapshots[len(snapshots)-1][1])

  return final_fragments

  
def distance_bc(work_dir,pop_tle,pop_nbm,plot=True,name_file='distribution_bc'):
  """ Give the distance between two distributions.

  INPUTS
  ------

  work_dir: string
    Working directory.
  pop_tle: dataframe
    Population from the TLE.
  pop_nbm: dataframe
    Population of the NBM.
  
  RETURN
  ------

  dist: float
    Distance between two distribution.

  """

  bc_nbm_list = pop_nbm['BC[m2/kg]'].astype(float).tolist()
  bc_tle_list = pop_tle['BC[m2/kg]'].astype(float).tolist()
  bc_nbm_area = get_function_area_bc(bc_nbm_list)
  bc_tle_area = get_function_area_bc(bc_tle_list)
  
  #Compute distance between two functions
  dist = 0
  ind_max = 0
  for i in range(0,len(bc_nbm_area)):
    d = abs(bc_nbm_area[i]-bc_tle_area[i])
    dist = dist + d

  if plot==True:

    log_am_nbm = map(lambda x : np.log10(float(x)/2.2),list(bc_nbm_list))
    log_am_tle = map(lambda x : np.log10(float(x)/2.2),list(bc_tle_list))

    fig = plt.figure(figsize=(9,7))
    ax1 = fig.add_subplot(2,1,1)
    ax1.spines["top"].set_visible(False)
    ax1.spines["right"].set_visible(False)
    ax1.set_ylabel('Number of debris',fontsize=14)
    n_nbm,b_nbm,pf = plt.hist(log_am_nbm,bins=100,label='NBM ('+str(len(bc_nbm_list))+')',range=(-5,5),alpha=0.6)
    n_tle,b_tle,pt   = plt.hist(log_am_tle,bins=100,label='TLE ('+str(len(bc_tle_list))+')',range=(-5,5),alpha=0.6)
    plt.legend(loc='upper right')

    ax2 = fig.add_subplot(2,1,2)
    ax2.spines["top"].set_visible(False)
    ax2.spines["right"].set_visible(False)
    ax2.set_xlabel('Log10 of the ratio aera over mass (m$^2$.kg$^{-1}$) / Distance = '+str(dist),fontsize=14)
    ax2.set_ylabel('Area',fontsize=14) 
    ax2.plot(b_nbm[1:-1],bc_nbm_area,label='NBM')
    ax2.plot(b_tle[1:-1],bc_tle_area,label='TLE')
    plt.legend(loc='upper left',numpoints=1)
  
    plt.savefig(work_dir+'/'+name_file)
    plt.close()
      
  return dist


def distance_apo(work_dir,pop_tle,pop_nbm,plot_distri=False,plot_gabbard=False,name_file='distribution_apo'):
  """ Give the distance between two distributions.

  INPUTS
  ------

  work_dir: string
    Working directory.
  pop_tle: dataframe
    Population from the TLE.
  pop_nbm: dataframe
    Population of the NBM.
  
  RETURN
  ------

  dist: float
    Distance between two distribution.

  """

  pop_nbm['Period[h]'] = 2*np.pi*np.sqrt(pop_nbm['a[m]']**3/earth_mu)/3600.
  pop_tle['Period[h]'] = 2*np.pi*np.sqrt(pop_tle['a[m]']**3/earth_mu)/3600.
  pop_nbm['Apogee-Re[km]']  = (pop_nbm['a[m]']*(1+pop_nbm['e'])-earth_eq_radius)/1000.
  pop_nbm['Perigee-Re[km]'] = (pop_nbm['a[m]']*(1-pop_nbm['e'])-earth_eq_radius)/1000.
  pop_tle['Apogee-Re[km]']  = (pop_tle['a[m]']*(1+pop_tle['e'])-earth_eq_radius)/1000.
  pop_tle['Perigee-Re[km]'] = (pop_tle['a[m]']*(1-pop_tle['e'])-earth_eq_radius)/1000.
  
  apo_nbm_list = pop_nbm['Apogee-Re[km]'].astype(float).tolist()
  apo_tle_list = pop_tle['Apogee-Re[km]'].astype(float).tolist()
  apo_nbm_area = get_function_area_apo(apo_nbm_list)
  apo_tle_area = get_function_area_apo(apo_tle_list)
  
  #Compute distance between two functions
  dist = 0
  ind_max = 0
  for i in range(0,len(apo_nbm_area)):
    d = abs(apo_nbm_area[i]-apo_tle_area[i])
    dist = dist + d

  if plot_distri==True:

    fig = plt.figure(figsize=(9,7))
    ax1 = fig.add_subplot(2,1,1)
    ax1.spines["top"].set_visible(False)
    ax1.spines["right"].set_visible(False)
    ax1.set_ylabel('Number of debris',fontsize=14)
    n_nbm,b_nbm,pf = plt.hist(apo_nbm_list,bins=100,label='NBM ('+str(len(apo_nbm_list))+')',range=(0,3000),alpha=0.6)
    n_tle,b_tle,pt = plt.hist(apo_tle_list,bins=100,label='TLE ('+str(len(apo_tle_list))+')',range=(0,3000),alpha=0.6)
    plt.legend(loc='upper right')

    ax2 = fig.add_subplot(2,1,2)
    ax2.spines["top"].set_visible(False)
    ax2.spines["right"].set_visible(False)
    ax2.set_xlabel('Apogee-Re [km] / Distance = '+str(dist),fontsize=14)
    ax2.set_ylabel('Area',fontsize=14) 
    ax2.plot(b_nbm[1:-1],apo_nbm_area,label='NBM')
    ax2.plot(b_tle[1:-1],apo_tle_area,label='TLE')
    plt.legend(loc='upper left',numpoints=1)
  
    plt.savefig(work_dir+'/'+name_file)
    plt.close()

  if plot_gabbard==True:
    
    fig, ax1= plt.subplots(figsize=(8,6))
    ax1.spines["top"].set_visible(False)
    ax1.spines["right"].set_visible(False)
    ax1.set_ylabel('Apogee/Perigee - Re [km]',fontsize=14)
    ax1.set_xlabel('Period [hour]',fontsize=14)
    ax1.plot(pop_tle['Period[h]'],pop_tle['Apogee-Re[km]'] ,'o',ms=3,c='gray',alpha=0.3,label='Apogee (TLE)')
    ax1.plot(pop_tle['Period[h]'],pop_tle['Perigee-Re[km]'],'o',ms=3,c='gray',alpha=0.3,label='Perigee (TLE)')
    ax1.plot(pop_nbm['Period[h]'],pop_nbm['Apogee-Re[km]'] ,'x',ms=5,c='b',alpha=0.7,label='Apogee (NBM)')
    ax1.plot(pop_nbm['Period[h]'],pop_nbm['Perigee-Re[km]'],'x',ms=5,c='r',alpha=0.7,label='Perigee (NBM)')
    plt.legend(loc='upper left')
    plt.savefig(work_dir+'/'+name_file+'_gabbard')
    plt.close()
        
  return dist


def get_function_area_bc(bc_list):
  """ Compute discrete integral of a set.

  INPUTS
  ------
    
  bc_list: list of float    
    Contains bc values.
    
  RETURN
  ------
  
  res_l: list of float
    List which contains int_[0,x] f(A/M) with x in log_am List
 
  """

  log_am = map(lambda x : np.log10(float(x)/2.2),bc_list)
  n,bins,patches = plt.hist(log_am,bins=100,range=(-5,5))
  plt.close()

  res_l = []
  res_val = 0
  l = bins[1]-bins[0]
  for i in range(1,len(n)):
    res_val = res_val + n[i]*l
    res_l.append(res_val)
    
  return res_l


def get_function_area_apo(apo_list):
  """ Compute discrete integral of a set.

  INPUTS
  ------
    
  apo_list: list of float    
    Contains apogee values.
    
  RETURN
  ------
  
  res_l: list of float
    List which contains int_[0,x] f(Apogee) with x in Apogee List
 
  """

  n,bins,patches = plt.hist(apo_list,bins=100,range=(0,3000))
  plt.close()

  res_l = []
  res_val = 0
  l = bins[1]-bins[0]
  for i in range(1,len(n)):
    res_val = res_val + n[i]*l
    res_l.append(res_val)
    
  return res_l


def plot_set_ephemeris(work_dir,list_ephemeris):
  """ Plot a set of ephemeris.
  
  INPUTS 
  ------

  work_dir: string
    Working directory.
  list_ephemeris: list of dataframes.
    List of ephemeris.

  """
  
  os.system('mkdir {0}/ephemeris'.format(work_dir))

  for i,ephemeris in enumerate(list_ephemeris):
    if (ephemeris.empty==False):
      fig, ax1= plt.subplots(figsize=(8,6))
      ax1.spines["top"].set_visible(False)
      ax1.spines["right"].set_visible(False)
      ax1.set_ylabel('a [km]',fontsize=14)
      ax1.set_xlabel('Time',fontsize=14)
      ax1.plot(ephemeris['Date'],ephemeris['a[m]'])
      ax1.plot(ephemeris['Date'],ephemeris['a[m]'],'o',ms=3,c='r')
      plt.savefig('{0}/ephemeris/ephemeris_{1}'.format(work_dir,i))
      plt.close()


def plot_set_snapshots(work_dir,snapshots):
  """ Plot a set of snapshots.
  
  INPUTS 
  ------

  work_dir: string
    Working directory.
  snapshots: list of datetime+dataframes.
    List of date and snapshots.

  """

  os.system('mkdir {0}/snapshots'.format(work_dir))
  
  for i,snapshot in enumerate(snapshots):
    date     = snapshot[0].strftime('%Y%m%d')
    snapshot = snapshot[1]
    snapshot['Period[h]'] = 2*np.pi*np.sqrt(snapshot['a[m]']**3/earth_mu)/3600.
    snapshot['Apogee-Re[km]']  = (snapshot['a[m]']*(1+snapshot['e'])-earth_eq_radius)/1000.
    snapshot['Perigee-Re[km]'] = (snapshot['a[m]']*(1-snapshot['e'])-earth_eq_radius)/1000.
    fig, ax1= plt.subplots(figsize=(8,6))
    ax1.spines["top"].set_visible(False)
    ax1.spines["right"].set_visible(False)
    ax1.set_ylabel('Apogee/Perigee - Re [km]',fontsize=14)
    ax1.set_xlabel('Period [hour]',fontsize=14)
    ax1.plot(snapshot['Period[h]'],snapshot['Perigee-Re[km]'],'o',ms=3,label='Perigee (NBM)')
    ax1.plot(snapshot['Period[h]'],snapshot['Apogee-Re[km]'] ,'x',ms=3,label='Apogee (NBM)')
    plt.legend(loc='upper left')
    plt.savefig('{0}/snapshots/gabbard_{1}_{2}'.format(work_dir,i,date))
    plt.close()           
    

def retropropagation(work_dir,local_db,snapshot_ref):
  """ Retropropagate a set of fragments to find the origin of the breakup.
 
  INPUTS
  ------
    
  work_dir: string 
    Working directory.
  local_db: object
    Object giving data about the local database.
  snapshot_ref: dataframe
    Snapshot of a set of fragments.

  """

  snapshot_ref['BC[m2/kg]'] = 0.1
  param = '../data/breakup/satlight_J2_SM_drag_retropropagation.cfg'
  date_i = snapshot_ref['Date'].iloc[0]
  date_f = date + timedelta(days=-10)
  delta_time = date_f-date_i
  os.system('sed -i -e "s/.*output_step_day.*/output_step_day = {0}/g" {1}'.format(0,param))
  os.system('sed -i -e "s/.*output_step_sec.*/output_step_sec = {0}/g" {1}'.format(3600,param))   
  os.system('sed -i -e "s/.*time_day.*/time_day = {0}/g" {1}'.format(delta_time.days,param))
  list_ephemeris = propagate(work_dir,snapshot_ref,param)
  snapshots = convert_ephemeris_2_snapshot(list_ephemeris)

  pop_dir_3d = work_dir+'/cloud_evolution_3d_retro'
  if os.path.isdir(pop_dir_3d):
    os.system('rm -r '+pop_dir_3d)
  os.system('mkdir '+pop_dir_3d)

  list_date = []
  list_mean_distance = []
      
  for snapshot in snapshots:
    date = snapshot[0]
    name = 'population_'+date.strftime('%Y%m%d%H%M%S')
    snapshot = df_kepl_2_cart(snapshot[1])    
    title = '{0} fragments at {1}'.format(len(snapshot),date.strftime('%Y/%m/%d %H:%M:%S'))
    fig = plt.figure()
    fig.suptitle(title)    
    ax = fig.add_subplot(111,projection='3d')
    dist = 1.5E6+earth_eq_radius
    ax.set_xlim(-dist,+dist)
    ax.set_ylim(-dist,+dist)
    ax.set_zlim(-dist,+dist)    
    ax.plot3D(snapshot['x[m]'],snapshot['y[m]'],snapshot['z[m]'],'ro',markersize=1)
    plt.savefig(pop_dir_3d+'/'+name)
    plt.close()

    if len(snapshot)>nb_ref:
      snapshot_ref = snapshot

    # Mean distance

    list_date.append(date)
    list_distance = []
    for i in range(0,len(snapshot),1):
      for j in range(0,len(snapshot),1):
        list_distance.append(np.sqrt((snapshot.iloc[i]['x[m]']-snapshot.iloc[j]['x[m]'])**2+(snapshot.iloc[i]['y[m]']-snapshot.iloc[j]['y[m]'])**2+(snapshot.iloc[i]['z[m]']-snapshot.iloc[j]['z[m]'])**2))
    list_mean_distance.append(np.mean(list_distance)/1000.)
      
  fig, ax1= plt.subplots(figsize=(8,6))
  ax1.spines["top"].set_visible(False)
  ax1.spines["right"].set_visible(False)
  ax1.set_xlabel('Time',fontsize=14)
  ax1.set_ylabel('Mean distance [km]',fontsize=14)
  plt.plot(list_date,list_mean_distance)
  plt.savefig(work_dir+'/mean_distance')
  plt.close()


##########################################################################

##########################################################################

###########################################################################
#
# Main
if __name__ == "__main__":

  #local_db = DB_env()
  
  parser = argparse.ArgumentParser(description='NASA Breakup Model')
  
  parser.add_argument("-c","--calibration",help="calibrate the NASA Breakup Model",action="store_true")
  parser.add_argument("-b","--breakup",help="performed a breakup and compare with TLE",action="store_true")
  parser.add_argument("-r","--retropropagation",help="retropropagate fragments to find the origin",action="store_true")

  # Options

  parser.add_argument("--parameters",help="file name of the NBM parameters",type=str)    
  parser.add_argument("--population",help="file name of the TLE population",type=str)  
  parser.add_argument("--event",help="file name of the breakup event",type=str) 
  parser.add_argument("-f","--folder",help="save data in a folder",action="store_true")
  parser.add_argument("-v","--verbosity",help="print informations about all the process",action="store_true")
  
  args = parser.parse_args()

  if args.folder:
    working_dir = True
  else:
    working_dir = False
    
  if working_dir==True:
    date = datetime.today()
    work_dir = 'temp_'+date.strftime('%H%M%S')
    if os.path.isdir(work_dir):
      os.system('rm -r '+work_dir)
    os.system('mkdir '+work_dir)
    work_dir = work_dir
  else:
    work_dir = '.'

  if args.calibration:
    if args.event and args.population:
      nbm_fitting(work_dir,args.population,args.event,verbosity=args.verbosity)
    else:
      print('Arguments are missing')
  if args.breakup:
    if args.event and args.population:
      breakup_comparison(work_dir,args.population,args.event,args.parameters,verbosity=args.verbosity)    
    else:
      print('Arguments are missing')
  if args.retropropagation:
    if args.population:
      retropropagation(work_dir,args.population)
    else:
      print('Arguments are missing')
