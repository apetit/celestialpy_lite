#!/usr/bin/python
# Licensed under a 3-clause BSD style license - see LICENSE.rst
# -*-coding:Utf-8 -*

"""
  propagation.py
  Author : Alexis Petit, Post-doc, IFAC-CNR, IMCCE
  Date : 2018 10 19
  This script is a part of CelestialPy. It allows to propagate 
  the orbit of a population of objects.
"""


#Packages


import argparse
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import time
import sys
import os
import copy
from datetime import datetime,timedelta
from multiprocessing import Process
import multiprocessing


#Packages celestialpy


sys.path.insert(0,os.path.abspath(os.path.join(os.path.dirname(__file__),'..')))

from celestialpy_lite import earth_eq_radius,earth_mu
from celestialpy_lite import propagate


#Code


def example_ic():
  """ Write an example of initial conditions.
  """

  name = 'TEST'
  idsat = 1
  sma = 7200.E3
  ecc = 0.1
  inc = 50.
  raan = 10.
  omega = 10.
  ma = 100.
  bc = 0.01
  date = datetime(2010,1,1,0,0,0)
  ic = []
  ic.append([name,idsat,date,sma,ecc,inc,raan,omega,ma,bc])  
  idsat = 2
  sma = 7400.E3
  ic.append([name,idsat,date,sma,ecc,inc,raan,omega,ma,bc])  
  headers = ['Name','Id Norad','Date','a[m]','e','i[deg]','RAAN[deg]','Omega[deg]','MA[deg]','BC[m2/kg]']
  ic = pd.DataFrame(ic,columns=headers)
  print 'Initial conditions:'
  print ic
  ic.to_csv('initial_conditions.csv',index=False)

  
def read_and_propagate(work_dir,name_file,param,para,verbosity,plot=False):
  """ Read a set of initial conditions and propagate them.

  INPUTS
  ------

  work_dir: string
    Working directory.
  name_file: string
    Name of the file containing the set of initial conditions.
  param: string
    File containing the paramaters of the orbit propagator.
  para: logical
    Flag for a parallel mode.
  plot: boolean
    Flag to plot the ephemeris.

  """

  path_op = '/Users/alexis/Documents/ODIN/softwares/nimastep-symplec'
  
  population = pd.read_csv(name_file)
  population['Date'] = pd.to_datetime(population['Date'],format='%Y-%m-%d %H:%M:%S')
  
  list_ephemeris = propagate(work_dir,path_op,population,param,para,verbosity)
  
  for i,ephemeris in enumerate(list_ephemeris):
    ephemeris.to_csv(work_dir+'/ephemeris_'+str(i)+'.csv',index=False,sep='\t')
    plot_orbit(work_dir+'/ephemeris_'+str(i),ephemeris)


def plot_orbit(name,ephemeris):
  """ Plot the ephemeris.

  INPUTS
  ------

  name: string
    Name of the plot.
  ephemeris: dataframe
    Ephemeris

  """ 

  ti = ephemeris.iloc[0]['Date']
  tf = ephemeris.iloc[len(ephemeris)-1]['Date']
  
  fig = plt.figure(figsize=(18,12))
  ax1 = fig.add_subplot(5,1,1)
  ax1.spines["top"].set_visible(False)
  ax1.spines["right"].set_visible(False)
  ax1.set_ylabel('a[km]',fontsize=14)        
  plt.plot(ephemeris['Date'],ephemeris['a[m]']/1000.)
  ax1.set_xlim([ti,tf])
  
  ax1 = fig.add_subplot(5,1,2)
  ax1.spines["top"].set_visible(False)
  ax1.spines["right"].set_visible(False)
  ax1.set_ylabel('e',fontsize=14)        
  plt.plot(ephemeris['Date'],ephemeris['e'])
  ax1.set_xlim([ti,tf])
  
  ax1 = fig.add_subplot(5,1,3)
  ax1.spines["top"].set_visible(False)
  ax1.spines["right"].set_visible(False)
  ax1.set_ylabel('i[deg]',fontsize=14)        
  plt.plot(ephemeris['Date'],ephemeris['i[deg]'])
  ax1.set_xlim([ti,tf])
  
  ax1 = fig.add_subplot(5,1,4)
  ax1.spines["top"].set_visible(False)
  ax1.spines["right"].set_visible(False)
  ax1.set_ylabel('RAAN[deg]',fontsize=14)        
  plt.plot(ephemeris['Date'],ephemeris['RAAN[deg]'])
  ax1.set_xlim([ti,tf])
  
  ax1 = fig.add_subplot(5,1,5)
  ax1.spines["top"].set_visible(False)
  ax1.spines["right"].set_visible(False)
  ax1.set_ylabel('Omega[deg]',fontsize=14)        
  plt.plot(ephemeris['Date'],ephemeris['Omega[deg]'])      
  ax1.set_xlim([ti,tf])
  
  plt.savefig(name+'.png')
  plt.close()
  

##########################################################################

##########################################################################

###########################################################################
#
# Main
if __name__ == "__main__":

  parser = argparse.ArgumentParser(description='Propagate a populations of RSO.')
  parser.add_argument("-p","--propagate",help="show informations about the TLE population",action="store_true")
  parser.add_argument("--name_file",help="file containing the initial conditions",type=str)
  parser.add_argument("--parallel",help="use the parallel mode",action="store_true")
  parser.add_argument("--param",help="parameter of the orbit propagator",type=str)
  parser.add_argument("--plot",help="plot the ephemeris",action="store_true")
  parser.add_argument("--ic",help="generate initial conditions",action="store_true")
  
  # Options

  parser.add_argument("-f","--folder",help="save data in a folder",action="store_true")
  parser.add_argument("--output",help="save data in a folder",type=str)
  parser.add_argument("-v","--verbosity",help="print informations about all the process",action="store_true")  
  args = parser.parse_args()

  if args.folder==True:
    if args.output:
      work_dir = args.output
    else:  
      date = datetime.today()
      work_dir = 'temp_'+date.strftime('%H%M%S')
    if os.path.isdir(work_dir):
      os.system('rm -r '+work_dir)
    os.system('mkdir '+work_dir)
  else:
    work_dir = '.'
    
  if args.propagate:
    if args.name_file:
      if args.param:
        read_and_propagate(work_dir,args.name_file,args.param,args.parallel,verbosity=args.verbosity,plot=args.plot)
      else:
        print('Error: parameters of the orbit propagator are missing')
    else:
      print('Error: initial conditions are missing')
  if args.ic:
    example_ic()
 
  

      
    
  
