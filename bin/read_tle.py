#!/usr/bin/python
# Licensed under a 3-clause BSD style license - see LICENSE.rst
# -*-coding:Utf-8 -*

"""
  read_tle.py
  Author: Alexis Petit, Post-doc, IFAC-CNR, IMCCE
  Date: 2018 10 19
  This script is a part of CelestialPy. It reads a TLE and it
  compares the result of an orbit propagator.
"""


#Packages


import argparse
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import time
import sys
import os
import copy
from datetime import datetime,timedelta
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()


#Packages celestialpy


sys.path.insert(0,os.path.abspath(os.path.join(os.path.dirname(__file__),'..')))

from celestialpy_lite import earth_eq_radius,earth_mu
from celestialpy_lite import propagate
from celestialpy_lite import TLE_data


#Code


def read_tle(name_file):
  """ Read TLE data.

  INPUTS
  ------

  name_file: string
    Name of the TLE file.

  RETURN
  ------

  observations: dataframe
    Orbital elements.

  """

  f = open(name_file,'r')
  d = f.readlines()
  f.close()

  tle_serie = []
  for row in d:
    tle_serie.append(row.strip())
  observations = TLE_data(tle_serie,format_tle='2_lines_serie').get_observations()

  return observations


def read_and_propagate(work_dir,name_file):
  """ Read from TLE data and propagate an orbit.

  INPUTS
  ------

  work_dir: string
    Working directory
  name_file: string
    Name of the TLE file.  

  """

  # Read and plot TLE data
  
  observations = read_tle(name_file)
  name = 'lageos'
  plot_orbit(name,observations)

  # Initial conditions before propagation
  
  observations = observations[observations['Date']>datetime(2000,1,1,0,0,0)]
  observations['Name'] = 'Lageos'
  observations['BC[m2/kg]'] = 0.1
  observations['Id Norad'] = 8820

  # Initialisation of the orbit propagator

  #path_op = '/Users/alexis/Documents/ODIN/softwares/nimastep-symplec'
  path_op = '/Users/alexis/Documents/ODIN/softwares/orbit-propagation-imcce'
  #path_op = '/home/alexis/ODIN/softwares/nimastep-symplec'
  #param = '../data/orbit_propagator/nimastep_J2_SM.cfg'
  param = '../data/orbit_propagator/satlight_J2-5_SM.cfg'

  dt = observations['Date'].iloc[len(observations)-1]-observations['Date'].iloc[0]
  days = dt.days
  os.system('sed -i -e "s/.*time_day.*/time_day = {0}/g" {1}'.format(days,param))

  # Propagation and plot the results
 
  ephemeris = propagate(work_dir,path_op,observations[0:1],param)[0]
  name = 'lageos_from_tle'
  plot_orbit(name,observations)
  name = 'lageos_from nimastep'
  plot_orbit(name,ephemeris)  

  
def plot_orbit(name,ephemeris):
  """ Plot the ephemeris.

  INPUTS
  ------

  name: string
    Name of the plot.
  ephemeris: dataframe
    Ephemeris

  """ 

  ti = ephemeris.iloc[0]['Date']
  tf = ephemeris.iloc[len(ephemeris)-1]['Date']
  
  fig = plt.figure(figsize=(18,12))
  ax1 = fig.add_subplot(5,1,1)
  ax1.spines["top"].set_visible(False)
  ax1.spines["right"].set_visible(False)
  ax1.set_ylabel('a[km]',fontsize=14)        
  plt.plot(ephemeris['Date'],ephemeris['a[m]']/1000.)
  ax1.set_xlim([ti,tf])
  
  ax1 = fig.add_subplot(5,1,2)
  ax1.spines["top"].set_visible(False)
  ax1.spines["right"].set_visible(False)
  ax1.set_ylabel('e',fontsize=14)        
  plt.plot(ephemeris['Date'],ephemeris['e'])
  ax1.set_xlim([ti,tf])
  
  ax1 = fig.add_subplot(5,1,3)
  ax1.spines["top"].set_visible(False)
  ax1.spines["right"].set_visible(False)
  ax1.set_ylabel('i[deg]',fontsize=14)        
  plt.plot(ephemeris['Date'],ephemeris['i[deg]'])
  ax1.set_xlim([ti,tf])
  
  ax1 = fig.add_subplot(5,1,4)
  ax1.spines["top"].set_visible(False)
  ax1.spines["right"].set_visible(False)
  ax1.set_ylabel('RAAN[deg]',fontsize=14)        
  plt.plot(ephemeris['Date'],ephemeris['RAAN[deg]'])
  ax1.set_xlim([ti,tf])
  
  ax1 = fig.add_subplot(5,1,5)
  ax1.spines["top"].set_visible(False)
  ax1.spines["right"].set_visible(False)
  ax1.set_ylabel('Omega[deg]',fontsize=14)        
  plt.plot(ephemeris['Date'],ephemeris['Omega[deg]'])      
  ax1.set_xlim([ti,tf])
  
  plt.savefig(name+'.png')
  plt.close()
  

##########################################################################

##########################################################################

###########################################################################
#
# Main
if __name__ == "__main__":

  name_file = '../data/TLE/lageos_8820.txt'
  work_dir = '.'
  read_and_propagate(work_dir,name_file)
  


 
  

      
    
  
