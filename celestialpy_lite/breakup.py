#Script Database from Python Dataframe
#!/usr/bin/python
# Licensed under a 3-clause BSD style license - see LICENSE.rst
# -*-coding:Utf-8 -*

"""
  breakup.py
  Authors : Alexis Petit, PhD Student, Namur University
            Quentin Jaubertie, intern, student, Namur University
  Date: 2018 03 05
  This script is a part of CelestialPy. It allows to create a
  cloud of space debris with the the NASA Breakup Model.
  Reference: Johnson et al, 2001, NASA's new breakup model of evolve 4.0
"""


#Packages


import sys
import os
import time
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from random import random
from jdcal import gcal2jd
from datetime import datetime,date,timedelta
from collections import OrderedDict


#Internal package


from celestialpy_lite.time_conversion import jd_2_date


#Code


__all__ = [
  'init_param_nbm',
  'read_param_nbm',
  'write_param_file',
  'write_schedule',
  'call_nbm'
]


def init_param_nbm(type_frag):
  """ Give the nominal parameters of the NASA Breakup Model.

  INPUTS
  ------

  type_frag: string
    Type of the fragmentation.

  RETURN
  ------

  param_nbm: dictionary
    Contain all the parameter of the NASA Breakup Model.
  
  """

  param_nbm = OrderedDict()
    
  limits = OrderedDict()
  limits["am_min"]   = 0.001
  limits["am_max"]   = 99.
  limits["size_min"] = 0.001
  limits["size_max"] = 4.
  limits["dv_max"]   = 1000.
  limits["coeff_am"] = 0.0
  param_nbm['limits'] = limits

  if (type_frag=='collision' or type_frag=='explosion' or type_frag=='unknown'):

    d_am_sc_alpha = OrderedDict()
    d_am_sc_alpha['alpha_sc_1']   =  0.0
    d_am_sc_alpha['alpha_sc_2_a'] =  0.3
    d_am_sc_alpha['alpha_sc_2_b'] =  0.4
    d_am_sc_alpha['alpha_sc_2_c'] =  1.2
    d_am_sc_alpha['alpha_sc_3']   =  1.0
    param_nbm['d_am_sc_alpha']    = d_am_sc_alpha

    d_am_sc_mu = OrderedDict()
    d_am_sc_mu['mu1_sc_1']   = -0.6
    d_am_sc_mu['mu1_sc_2_a'] = -0.6
    d_am_sc_mu['mu1_sc_2_b'] = -0.318
    d_am_sc_mu['mu1_sc_2_c'] =  1.1
    d_am_sc_mu['mu1_sc_3']   = -0.95
    d_am_sc_mu['mu2_sc_1']   = -1.2
    d_am_sc_mu['mu2_sc_2_a'] = -1.2
    d_am_sc_mu['mu2_sc_2_b'] = -1.333
    d_am_sc_mu['mu2_sc_2_c'] =  0.7
    d_am_sc_mu['mu2_sc_3']   = -2.0
    param_nbm['d_am_sc_mu']  = d_am_sc_mu

    d_am_sc_sigma = OrderedDict()
    d_am_sc_sigma['sigma1_sc_1'] =   0.1
    d_am_sc_sigma['sigma1_sc_2_a'] = 0.1
    d_am_sc_sigma['sigma1_sc_2_b'] = 0.2
    d_am_sc_sigma['sigma1_sc_2_c'] = 1.3
    d_am_sc_sigma['sigma1_sc_3'] =   0.3
    d_am_sc_sigma['sigma2_sc_1'] =   0.5
    d_am_sc_sigma['sigma2_sc_2_a'] = 0.5
    d_am_sc_sigma['sigma2_sc_2_b'] = -1.0
    d_am_sc_sigma['sigma2_sc_2_c'] = 0.5
    d_am_sc_sigma['sigma2_sc_3'] =   0.3
    param_nbm['d_am_sc_sigma'] = d_am_sc_sigma

  elif (type_frag=='rocket_body'):

    d_am_rb_alpha = OrderedDict()
    d_am_rb_alpha['alpha_rb_1']   =  1.0
    d_am_rb_alpha['alpha_rb_2_a'] =  1.0
    d_am_rb_alpha['alpha_rb_2_b'] =  -0.3571
    d_am_rb_alpha['alpha_rb_2_c'] =  1.4
    d_am_rb_alpha['alpha_rb_3']   =  0.5
    param_nbm['d_am_rb_alpha']    = d_am_rb_alpha

    d_am_rb_mu = OrderedDict()
    d_am_rb_mu['mu1_rb_1']   = -0.45
    d_am_rb_mu['mu1_rb_2_a'] = -0.45
    d_am_rb_mu['mu1_rb_2_b'] = -0.9
    d_am_rb_mu['mu1_rb_2_c'] =  0.5
    d_am_rb_mu['mu1_rb_3']   = -0.9
    d_am_rb_mu['mu2_rb']     = -0.9
    param_nbm['d_am_rb_mu']  = d_am_rb_mu

    d_am_rb_sigma = OrderedDict()
    d_am_rb_sigma['sigma1_rb']     = 0.55
    d_am_rb_sigma['sigma2_rb_1'] =   0.28
    d_am_rb_sigma['sigma2_rb_2_a'] = 0.28
    d_am_rb_sigma['sigma2_rb_2_b'] = -0.1636
    d_am_rb_sigma['sigma2_rb_2_c'] = 1.0
    d_am_rb_sigma['sigma2_rb_3'] =   0.1
    param_nbm['d_am_rb_sigma'] = d_am_rb_sigma
   
  else :
      
    print('Bad type of fragmentation : ',type_frag)
    sys.exit()

  if (type_frag=='explosion' or type_frag=='rocket_body' or type_frag=='unknown'):

    d_dv = OrderedDict()
    d_dv['sigma_exp'] = 0.4
    d_dv['mu_a_exp']  = 0.2
    d_dv['mu_b_exp']  = 1.85
    d_dv['coeff_vel'] = 1.0
    param_nbm['d_dv'] = d_dv

  elif (type_frag=='collision'):

    d_dv = OrderedDict()
    d_dv['sigma_exp'] = 0.4
    d_dv['mu_a_exp']  = 0.9
    d_dv['mu_b_exp']  = 2.9
    d_dv['coeff_vel'] = 1.0
    param_nbm['d_dv'] = d_dv
    
  else :
      
    print('Bad type of fragmentation : ',type_frag)
    sys.exit()  
    
  return param_nbm


def read_param_nbm(name_file,type_frag):
  """ From a file, read the parameters of the NBM.

  INPUTS
  ------

  name_file: string
    File containing the parameters.
  type_frag: string
    Type of fragmentation.

  RETURN
  ------

  param_nbm: dictionary
    Parameters of the NBM.

  """

  f = open(name_file,'r')
  data = f.readlines()
  f.close()
  
  param_nbm = OrderedDict()

  limits = OrderedDict()
  for i,item in enumerate(data[0].split()):
    limits[item] = float(data[1].split()[i])
  param_nbm['limits'] = limits

  if (type_frag=='explosion' or type_frag=='unknown'):

    d_am_sc_alpha = OrderedDict()
    for i,item in enumerate(data[2].split()):
      d_am_sc_alpha[item] = float(data[3].split()[i])
    param_nbm['d_am_sc_alpha']    = d_am_sc_alpha

    d_am_sc_mu = OrderedDict()
    for i,item in enumerate(data[4].split()):
      d_am_sc_mu[item] = float(data[5].split()[i])
    param_nbm['d_am_sc_mu']  = d_am_sc_mu

    d_am_sc_sigma = OrderedDict()
    for i,item in enumerate(data[6].split()):
      d_am_sc_sigma[item] = float(data[7].split()[i])
    param_nbm['d_am_sc_sigma'] = d_am_sc_sigma

    d_dv = OrderedDict()
    for i,item in enumerate(data[8].split()):
      d_dv[item] = float(data[9].split()[i])
    param_nbm['d_dv'] = d_dv

  elif (type_frag=='rocket_body'):

    d_am_rb_alpha = OrderedDict()
    for i,item in enumerate(data[2].split()):
      d_am_rb_alpha[item] = float(data[3].split()[i])
    param_nbm['d_am_rb_alpha']    = d_am_rb_alpha

    d_am_rb_mu = OrderedDict()
    for i,item in enumerate(data[4].split()):
      d_am_rb_mu[item] = float(data[5].split()[i])
    param_nbm['d_am_rb_mu']  = d_am_rb_mu

    d_am_rb_sigma = OrderedDict()
    for i,item in enumerate(data[6].split()):
      d_am_rb_sigma[item] = float(data[7].split()[i])
    param_nbm['d_am_rb_sigma'] = d_am_rb_sigma

    d_dv = OrderedDict()
    for i,item in enumerate(data[8].split()):
      d_dv[item] = float(data[9].split()[i])
    param_nbm['d_dv'] = d_dv

  else:

    print('Bad type of fragmentation : ',type_frag)
    sys.exit()  
    
  return param_nbm
  

def write_param_file(name_dir,type_frag,id_norad,param):
  """ Write the parameter file of the NASA Breakup Model.

  INPUTS
  ------

  name_dir: string
    Directory containing the parameter file.
  type_frag: string
    Type of the fragmentation.
  param: dictionary
    Contain the parameter of the NASA Breakup Model.

  RETURN
  ------

  name: string
    File name of the parameter files.

  """

  if not (type_frag=='explosion' or type_frag=='rocket_body'):     
    print('Bad choice of the fragmentation type with ',type_frag)
    sys.exit()

  name = name_dir+'/nbm_param_'+str(id_norad)+'.txt'
  f = open(name,'w')    
  for key in param:
    for key_scalar in param[key]:
      f.write('{0:12} '.format(key_scalar) )
    f.write('\n' )
    for key_scalar in param[key]:
       f.write('{0:12} '.format(param[key][key_scalar])  )     
    f.write('\n' )
  f.close()

  return name


def write_schedule(work_dir,events,name_file='schedule.txt'):
  """ Write the schedule for NIMASTEP/SYMPLEC.
 
  INPUTS
  ------

  work_dir: string
    Working directory.
  events: dataframe
    Contain all fragmentation events.

  OPTIONAL INPUTS
  ---------------

  name_file: string
    Name of the schedule file.

  RETURN
  ------

  name_file: string
    Name of the schedule file.

  """

  name_file = work_dir+'/'+name_file

  f = open(name_file,'w')
  f.write('{0:8},{1:12},{2:4},{3:2},{4:2},{5:35}  ,{6:15}  ,{7:10},{8:10},{9:10},{10:10},{11:10},{12:10},{13:10},{14:4} \n'.format('IdSat','Julian day','YYYY','MM','DD','Name','Type','a[m]','e','i[deg]','RAAN[deg]','Omega[deg]','M[deg]','Mass[kg]','S'))

  events['Date'] = pd.to_datetime(events['Date'],format='%Y-%m-%d %H:%M:%S')
  for k in range(0,len(events),1):
    row     = events.iloc[k]
    idnorad = "%08d" % float(row['Id Norad'])
    date = row['Date']
    yy      = int(date.strftime('%Y'))
    mm      = int(date.strftime('%m'))
    dd      = int(date.strftime('%d'))
    hh      = int(date.strftime('%H'))
    mi      = int(date.strftime('%M'))
    ss      = int(date.strftime('%S'))
    sec     = hh*3600.+mi*60.+ss
    day_dec = sec/86400.    
    jd      = "%012.3f" % float(np.sum(gcal2jd(yy,mm,dd))+day_dec)
    name    = row['Name'].strip().replace(' ','_').replace('/','_')
    
    sma = "%010.3d" % float(row['a[m]']) 
    ecc = "%010.8f" % float(row['e'])
    inc = "%07.3f"  % float(row['i[deg]'])
    if row['RAAN[deg]']==np.nan:
      raan = "%07.3f"  % float(360.*random())
    else:
      raan = "%07.3f"  % float(row['RAAN[deg]'])
    if row['Omega[deg]']==np.nan:
      omega = "%07.3f"  % float(360.*random())
    else:
      omega = "%07.3f"  % float(row['Omega[deg]'])
    if row['MA[deg]']==np.nan:
      ma = "%07.3f"  % float(360.*random())
    else:
      ma = "%08.2f"  % float(row['MA[deg]'])
    if row['Mass[kg]']==np.nan:
      mass = "%08.2f"  % float(100.)
    else:
      mass = "%08.2f"  % float(row['Mass[kg]'])
    s = "%04.3f" % float(row['S'])
    type_frag = row['Type']
    f.write('{0:8},{1:12},{2:4},{3:2},{4:2},{5:35}  ,{6:15}  ,{7:10},{8:10},{9:10},{10:10},{11:10},{12:10},{13:10},{14:4}\n'.format(str(idnorad),jd,yy,mm,dd,name,type_frag,sma,ecc,inc,raan,omega,ma,mass,s))
  f.close()

  name_file = work_dir+'/schedule.txt'
  
  return name_file

  
def call_nbm(nbm_path,schedule_file,nbm_param_file,output='breakup',clean=True,verbosity=False):
  """ Compute the NASA Breakup Model.

  INPUTS
  ------

  nbm_path: string
    Path of the directory containing the NASA Breakup Model.
  schedule_file: string
    Name of the scheduler.
  nbm_param_file: string
    Name of file containing the parameters.

  OPTIONAL INPUTS
  ---------------  

  output: string
    Name of the local directory.
  clean: logical
    Say if we remove the local directoty.
  verbosity: logical
    Say if we print informations on the screen.      

  RETURN
  ------
  
  fragments: dataframe
    Data about the fragments generated by the NBM.

  """
 
  start_time = time.time()
  if verbosity==True:
    print('NBM start')

  # Copy and launch the NBM

  if os.path.isdir(output):
    os.system('rm -r '+output)
  os.system('mkdir '+output)
  os.system('mkdir '+output+'/inputs')
  os.system('mkdir '+output+'/outputs')
  os.system('cp '+schedule_file+'  '+output+'/inputs/schedule.txt')
  os.system('cp '+nbm_param_file+' '+output+'/inputs/')
  os.system('cp '+nbm_path+'/bin/breakup '+output+'/')
  if verbosity==True:
    os.system('cd '+output+'; ./breakup')
  else:
    os.system('cd '+output+'; ./breakup > log_nbm.txt')
  
  # Read orbital elements

  f = open(output+'/outputs/cloud.txt','r')
  data = f.readlines()
  f.close()
  
  fragments = []
  headers = data[0].strip().split()
  for row in data[1:]:
    items = row.strip().split()
    fragments.append(items)
  fragments = pd.DataFrame(fragments,columns=headers)
  float_item = ['Julian_Day','a[m]','e','i[deg]','RAAN[deg]','Omega[deg]','MA[deg]','BC[m2/kg]','Mass[kg]','Size[m]']
  fragments[float_item] = fragments[float_item].astype(float)
  fragments['Date'] = pd.to_datetime(fragments['Julian_Day'].map(lambda x: jd_2_date(x))) 
  fragments['Id Norad'] = fragments['NORAD'].astype(int)
  fragments.drop(['NORAD'], axis=1)
  
  # Increment of velocity
  
  f = open(output+'/outputs/cloud_dv.txt','r')
  data = f.readlines()
  f.close()

  dv = [] 
  headers = data[0].strip().split()
  for row in data[1:]:
    items = row.strip().split()
    dv.append(items)
  dv = pd.DataFrame(dv,columns=headers)
  dv[headers] = dv[headers].astype(float)
  
  final_time = time.time()
  
  if clean==True:
    os.system('rm -r '+output)
  if verbosity==True:   
    print('NBM end with ',final_time-start_time)
  
  return fragments,dv

  


