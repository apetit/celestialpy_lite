#!/usr/bin/env python3
# Licensed under a 3-clause BSD style license - see LICENSE.rst
# -*-coding:Utf-8 -*

"""
  coordinates.py
  Authors: Alexis Petit, PhD Student, Namur University
  Date: 2017 03 09
  This script is a part of CelestialPy. It allows to perform transformations 
  between different frames.
"""


#Packages


import math
import numpy as np
from numpy import cos,sin,sqrt,dot,cross
from numpy.linalg import norm
from math import acos,asin,atan,atan2,floor,fmod,isinf,isnan
from scipy.constants import pi
from scipy.interpolate import interp1d
import sys
import pandas as pd
import copy
import os
import sys


#Internal packages


from celestialpy_lite.constants import earth_eq_radius
from celestialpy_lite.constants import earth_J2
from celestialpy_lite.constants import earth_mu
from celestialpy_lite.constants import earth_omega
from celestialpy_lite.constants import e_earth
from celestialpy_lite.time_conversion import date_2_jd
from celestialpy_lite.time_conversion import jd_2_jd_cnes
from celestialpy_lite.time_conversion import greenwich_mean_sideral_time


#Code


MAX_ITERATIONS = 100


__all__ = [
    'cart_2_kepl',
    'kepl_2_cart',
    'df_kepl_2_cart',
    'df_cart_2_kepl',
    'cart_2_sph',
    'sph_2_cart', 
    'kepl_2_equinoctial',
    'mean_anomaly_2_ecc_anomaly',
    'true_anomaly_2_ecc_anomaly',
    'ecc_anomaly_2_mean_anomaly',
    'true_anomaly_2_mean_anomaly',
    'ecc_anomaly_2_true_anomaly',
    'mean_anomaly_2_true_anomaly',
    'flight_path_angle',
    'dms_2_rad',
    'hms_2_rad',
    'degree_2_hms',
    'degree_2_dms',
    'geocentric_2_topocentric',
    'topocentric_2_geocentric',
    'latlon_2_ecef',
    'ecef_2_latlon',
    'rotation',
    'convert_ephemeris_2_snapshot',
    'coord_interpol',
    'ntw_frame'
]


def cart_2_kepl(cart,mu,angle='rad'):
  """ Give the keplerian elements from cartesian coordinates.

  INPUTS
  ------

  cart: float array
    Cartesian coordinates (r,v).
  mu: float
    Gravitational constant time the mass of the central body.
  angle: string
    Unit of the angles.

  RETURN
  ------

  kepl: float array
    Keplerian elements (a,e,i,raan,omega,ma).

  """

  x = cart[0]
  y = cart[1]
  z = cart[2]
  xp = cart[3]
  yp = cart[4]
  zp = cart[5]
   
  r   = np.sqrt(x**2+y**2+z**2)
  usa = 2./r-(xp**2+yp**2+zp**2)/mu
  sma  = 1./usa
  pn  = usa*np.sqrt(mu*usa)

  c1   = x*yp-y*xp
  c2   = y*zp-z*yp
  c3   = x*zp-z*xp
  w4   = c2**2+c3**2
  ccar = w4+c1**2
  rcar = np.sqrt(ccar)

  uh = 1./rcar
  if (w4>ccar*1.E-12):
    chi  = c1*uh
    sinhi = np.sqrt(1.-chi**2)
    inc    = acos(chi)
    raan  = math.atan2(c2,c3)
    raan  = (raan+2.*np.pi)%(2.*np.pi)      
  else:
    inc   = 0.
    sinhi = 0.
    if (c1<0.):
      inc = np.pi
    raan = 0.

  pp = ccar/mu
  tgbeta = (x*xp+y*yp+z*zp)*uh
  pusa = pp*usa
  ecc = np.sqrt(1.-pusa)

  #On limite la valeur de e a 10e-6

  iie = int(ecc*1000000.)
  if (iie==0):
    ecc = 0.
      
  esinu = tgbeta*np.sqrt(pusa)
  ecosu = 1.-r*usa
  u = math.atan2(esinu,ecosu)
  anom = u-esinu
  anom = (anom+2.*np.pi)%(2.*np.pi)

  w1  = pp-r
  cin = pp*tgbeta
  v   = math.atan2(cin,w1)

  w3      = x*np.cos(raan)+y*np.sin(raan)
  qos     = w3*sinhi
  vpomega = math.atan2(z,qos)
    
  if (iie==0):
    omega  = 0.
    anom = vpomega
  else:
    omega = vpomega-v
    omega = (omega+2.*pi)%(2.*np.pi)
  
  kepl = np.zeros(6)   
  kepl[0] = sma
  kepl[1] = ecc
  if angle=='rad':
    kepl[2] = inc   
    kepl[3] = (raan+2.*np.pi)%(2.*np.pi)
    kepl[4] = (omega+2.*np.pi)%(2.*np.pi)
    kepl[5] = (anom+2.*np.pi)%(2.*np.pi)
  elif angle=='deg':
    kepl[2] = inc*180/np.pi
    kepl[3] = ((raan+2.*np.pi)%(2.*np.pi))*180/np.pi
    kepl[4] = ((omega+2.*np.pi)%(2.*np.pi))*180/np.pi
    kepl[5] = ((anom+2.*np.pi)%(2.*np.pi))*180/np.pi
  else:
    print('Bad angle unit')
    sys.exit()
      
  return kepl
   

def kepl_2_cart(kepl,mu,angle='rad'):
  """ Give the cartesian coordinates from keplerian elements.

  INPUTS
  ------

  kepl: array
    Keplerian elements (a,e,i,raan,omega,ma).
  mu: float
    Gravitational constant time the mass of the central body.
  angle: string
    Unit of the angles.

  RETURN
  ------
 
  cart: array
    Cartesian coordinates (x,v)  

  """
    
  sma  = kepl[0]
  ecc  = kepl[1]
  if angle=='rad':
    inc   = kepl[2]
    raan  = kepl[3]
    omega = kepl[4]
    anom  = (2*np.pi+kepl[5])%(2*np.pi)
  elif angle=='deg':
    inc   = kepl[2]*np.pi/180.
    raan  = kepl[3]*np.pi/180.
    omega = kepl[4]*np.pi/180.
    anom  = (2*np.pi+kepl[5]*np.pi/180.)%(2*np.pi)     
  else:
    print('Bad angle unit')
    sys.exit()
      
  if ecc>=0 and ecc<1:
    ecc2 = ecc*ecc
    snraan = np.sin(raan)
    csraan = np.cos(raan)
    snomega = np.sin(omega)
    csomega = np.cos(omega)
    cshi = np.cos(inc)
    snhi = np.sqrt(1.0-cshi*cshi)
    prod = cshi*snomega
    
    px = sma*(-snraan*prod+csraan*csomega)
    py = sma*( csraan*prod+snraan*csomega)
    pz = sma*snhi*snomega
    
    b = sma*np.sqrt(1.0-ecc2)
    prod = cshi*csomega
    
    qx = b*(-snraan*prod-csraan*snomega)
    qy = b*( csraan*prod-snraan*snomega)
    qz = b*  snhi*csomega
    
    u = mean_anomaly_2_ecc_anomaly(ecc,anom)
   
    cosu   = np.cos(u)
    ucosme = cosu-ecc
    sinu   = np.sin(u)
    
    x = ucosme*px+sinu*qx
    y = ucosme*py+sinu*qy
    z = ucosme*pz+sinu*qz
    
    pn = np.sqrt(earth_mu/sma)/sma
    c1 = pn/(1.0-ecc*cosu)
    
    xp = c1*(-px*sinu+qx*cosu)
    yp = c1*(-py*sinu+qy*cosu)
    zp = c1*(-pz*sinu+qz*cosu)

    cart = np.zeros(6)
    cart[0] = x
    cart[1] = y
    cart[2] = z
    cart[3] = xp
    cart[4] = yp
    cart[5] = zp

  else:
    print('Conversion keplerian coordinates to cartesians coordinates')
    print('Not elliptical case : e = ',ecc)
      
  return cart


def df_kepl_2_cart(ic_kepl,unit='deg'):
  """ Convert a dataframe of orbital elements in cartesian coordinates.

  INPUT
  -----

  ic_kepl: dataframe
    Orbital elements

  RETURN
 
  ic_cart: dataframe
    Cartesian coordinates

  """

  cart = []
  headers = ['x[m]','y[m]','z[m]','vx[m/s]','vy[m/s]','vz[m/s]']
  for i in range(0,len(ic_kepl),1):
    kepl = ic_kepl.iloc[i][['a[m]','e','i[deg]','RAAN[deg]','Omega[deg]','MA[deg]']].tolist()
    kepl[2] = kepl[2]*np.pi/180.  
    kepl[3] = kepl[3]*np.pi/180.
    kepl[4] = kepl[4]*np.pi/180.
    kepl[5] = kepl[5]*np.pi/180.
    cart.append(kepl_2_cart(kepl,earth_mu))
  cart = pd.DataFrame(cart,columns=headers)
  ic_cart = pd.concat([ic_kepl,cart],axis=1)

  return ic_cart


def df_cart_2_kepl(ic_cart,unit='deg'):
  """ Convert a dataframe of cartesian coordinates to orbital elements.

  INPUT
  -----

  ic_cart: dataframe
    Cartesian coordinates.

  RETURN
 
  ic_kepl: dataframe
    Orbital elements.

  """

  ic_kepl = []
  headers = ['a[m]','e','i[deg]','RAAN[deg]','Omega[deg]','MA[deg]']
  for i in range(0,len(ic_cart),1):
    cart = ic_cart.iloc[i][['x[m]','y[m]','z[m]','vx[m/s]','vy[m/s]','vz[m/s]']].tolist()
    kepl = cart_2_kepl(cart,earth_mu)
    kepl[2] = kepl[2]*180./np.pi  
    kepl[3] = kepl[3]*180./np.pi
    kepl[4] = kepl[4]*180./np.pi
    kepl[5] = kepl[5]*180./np.pi
    ic_kepl.append(kepl)    
  ic_kepl = pd.DataFrame(ic_kepl,columns=headers)
  ic_kepl = pd.concat([ic_cart,ic_kepl],axis=1)

  return ic_kepl


def cart_2_sph(cart):
  """ Compute the spherical coordinates from cartesians coordinates.

  INPUTS
  ------

  cart: array of float
    Cartesian coordinates [m,m/s]

  RETURN
  -------

  kepl: array
    Spherical coordinates (alt,long,lat,dalt,dlong,dlat) 
 
  """

  sph = np.zeros(6)
  r = cart[0]**2.0+cart[1]**2.0
  #check if we have a degenerate case
  if r>0 :     
    sph[0] = np.sqrt(cart[0]**2.0+cart[1]**2.0+cart[2]**2.0)
    sph[1] = math.atan2(cart[1],cart[0])%(2*np.pi)
    sph[2] = math.asin(cart[2]/sph[0])
  else:
    sph[1] = 0
    if cart[2]==0:
        sph[2]=0
    else:
      if cart[2]>0:
        sph[2]=np.pi/2.
      else:
        sph[2]=-np.pi/2.
    r = np.abs(cart[2])     
  sph[3] = (cart[0]*cart[3]+cart[1]*cart[4]+cart[2]*cart[5])/sph[0]
  sph[4] = (cart[0]*cart[4]-cart[3]*cart[1])/(cart[0]**2.0+cart[1]**2.0) 
  sph[5] = (cart[5]*(cart[0]**2.0+cart[1]**2.0)-cart[2]*(cart[0]*cart[3]+cart[1]*cart[4])) / (sqrt(cart[0]**2.0+cart[1]**2.0)*(cart[0]**2.0+cart[1]**2.0+cart[2]**2.0))

  return sph


def sph_2_cart(sph):
    """ Compute the cartesian coordinates from spherical coordinates.
    
    INPUT
    -----

    sph: float-array
      Spherical coordinates (alt,long,lat,dalt,dlong,dlat)

    RETURN
    ------

    cart: float-array
      Cartesian coordinates (x,v)
   
    """

    cart = np.zeros(6)
    cart[0] = sph[0]*cos(sph[1])*sin(sph[2]) 
    cart[1] = sph[0]*sin(sph[1])*sin(sph[2])
    cart[2] = sph[0]*cos(sph[2])             
    cart[3] = sph[3]*(cos(sph[2])*cos(sph[1]))-sph[0]*( sph[5]*sin(sph[2])*cos(sph[1])+sph[4]*cos(sph[2])*sin(sph[1]))
    cart[4] = sph[3]*(cos(sph[2])*sin(sph[1]))+sph[0]*(-sph[5]*sin(sph[2])*sin(sph[1])+sph[4]*cos(sph[2])*cos(sph[1]))
    cart[5] = sph[3]*sin(sph[2])+sph[0]*sph[5]*cos(sph[2])
  
    return cart


def kepl_2_equinoctial(kepl,mu):
    """ Compute the equinoctial coordinates from keplerian elements.
    Vallado, 2012, p. 108
    
    INPUTS
    ------

    kepl: float array
      Keplerian elements (a,e,i,raan,arg_pe,ma) in meters and degrees.
    mu: float
      Gravitational constant time the Earth mass.    

    RETURN
    ------

    equinoc: float array
      Equinoctial coordinates (a,k,h,l,p,q) 
 
    """

    #check if retrograde
    if i>90:
      f = -1
    else:
      f = 1
        
    equinoc = np.zeros(6)  
    equinoc[0] = kepl[0]                                   
    equinoc[1] = kepl[1]*cos(kepl[3]+f*kepl[4])
    equinoc[2] = kepl[1]*cos(kepl[3]+f*kepl[4])
    equinoc[3] = kepl[5]+kepl[4]+kepl[3]
    equinoc[4] = sin(kepl[2])*sin(kepl[3])/(1+cos(kepl[2])**f)
    equinoc[5] = sin(kepl[2])*cos(kepl[3])/(1+cos(kepl[2])**f)

    return equinoc


def cart_2_flight_elements(cart):
    """ Compute the flight elements from the cartesian elements.

    INPUTS
    ------

    cart: float array
      Cartesian coordinates
 
    RETURN
    ------

    flight_elem: float array
      Flight elements

    """

    flight_elem = np.zeros(6)

    #position and velocity magnitude
    r = np.sqrt(cart[0]**2+cart[1]**2+cart[2]**2)
    v = np.sqrt(cart[3]**2+cart[4]**2+cart[5]**2)

    #right ascension (alpha)
    alpha = np.acos(cart[0]/np.sqrt(cart[0]**2+cart[1]**2))
    
    #declination (delta)    
    delta = np.asin(cart[2]/r)

    #matrice to transform a vector in the SEZ system
    m = np.zeros(3,3)
    m[0,0] = np.cos(alpha)*np.cos(delta)
    m[0,1] = np.sin(alpha)*np.cos(delta)
    m[0,2] = -np.sin(delta)
    m[1,0] = -np.sin(alpha)
    m[1,1] = np.cos(alpha)
    m[1,2] = 0.0    
    m[1,0] = np.sin(delta)*np.cos(alpha)
    m[1,1] = np.sin(alpha)*np.sin(delta)
    m[1,2] = np.cos(delta)
    v_sez = m*cart[3:5]

    #flight-path angle from the local horizontal
    phi = np.acos(v_sez[2]/v)
    
    #azimut (beta)
    beta = np.acos(-v_sez[0]/np.sqrt(v_sez[0]**2+v_sez[1]**2))
    
    flight_elem[0] = r 
    flight_elem[1] = v
    flight_elem[2] = alpha   
    flight_elem[3] = delta
    flight_elem[4] = phi
    flight_elem[5] = beta   

    return flight_elem


def mean_anomaly_2_ecc_anomaly(e,ma,precision=1e-14,unit='radian'):
    """ Compute the eccentric anomaly from the mean anomaly.
    
    INPUTS
    ------

    ecc: float
      Excentricity
    ma: float
      Mean anomaly

    RETURN
    ------

    ea: float
      Eccentric anomaly

    """

    if (e<0):

      print('Error with the eccentricity: e < 0')
  
    elif (e>1):
    
      print('Error with the eccentricity: e > 1')
      
    else:

      U = 0.
      e2 = e*e
      ANOM0 = (2*np.pi+ma)%(2*np.pi)
      
      ee2 = e2*0.5
      ee3 = ee2*e*0.25
      U = ANOM0+(e-ee3)*np.sin(ANOM0)+ee2*np.sin(2.*ANOM0)+3.*ee3*np.sin(3.*ANOM0)
      tol = 5.e-13
      correc = 1.
      niter = 0

      while (np.abs(correc)>tol):
        CORREC = (ANOM0-(U-e*np.sin(U)))/(1.-(e*np.cos(U)))
        U = U + CORREC
        niter = niter + 1
        if (niter>=100): 
          #print "Nombre maximal d'etapes de resolution de l'equation de kepler atteint : 100"
          break
      
      #We solve Kepler's equation (Vallado, 2014, p.65)
       
      #if (((ma<0) and (ma>-np.pi)) or(ma>np.pi)):
      #  ea0 = ma - ecc
      #else:
      #  ea0 = ma + ecc
      
      #ea=1E6
      #while(np.abs(ea-ea0)>precision):
      #  ea0 = ea 
      #  ea = ea0 + (ma-ea0+ecc*sin(ea0))/(1-ecc*cos(ea0))
                  
    return U


def true_anomaly_2_ecc_anomaly(ecc,nu,unit='radian'):
  """ Compute the eccentric anomaly from the true anomaly.

  INPUTS
  ------

  ecc: float
    Eccentricity
  nu: float
    True anomaly
  unit: string
    Unit of the angle.

  RETURN
  ------

  ea: float
    Eccentric anomaly
  
  """

  if unit=='radian':
    pass
  elif unit=='degree':
    nu = nu*np.pi/180.
  else:
    print('Bad type of unit')

  ea = atan2(sqrt(1-ecc**2)*sin(nu),ecc+cos(nu))
  ea = fmod(ea,2*np.pi)

  if unit=='degree':
    ea = ea*180./np.pi  
    
  return ea


def ecc_anomaly_2_mean_anomaly(ecc,ea):
    """ Compute the mean anomaly from the eccentric anomaly.
    
    INPUTS
    ------

    ecc: float
      Eccentricity
    ea: float
      Eccentric anomaly    

    RETURN
    ------

    ma: float
      Mean anomaly
 
    """

    ma = ea-ecc*sin(ea)
    ma = fmod(ma,2*np.pi)
    if ma<0:
      ma += 2*np.pi
    
    return ma


def true_anomaly_2_mean_anomaly(ecc,nu):
  """ Compute mean anomaly from the true anomaly.

  INPUTS
  ------

  ecc: float
    Eccentricity
  nu: float
    True anomaly

  RETURN
  ------

  ea: float
    Eccentric anomaly

  """

  ea = true_anomaly_2_ecc_anomaly(ecc,nu)
  ma = ecc_anomaly_2_mean_anomaly(ecc,ea)
  ma = fmod(ma,2*np.pi)
    
  return ma


def ecc_anomaly_2_true_anomaly(ecc,ea):
  """ Compute the true anomaly from the eccentric anomaly.
    
  INPUTS
  ------

  ecc: float
    Eccentricity
  ea: float
    Eccentric anomaly

  RETURN
  ------

  nu: float
    True anomaly

  """
    
  ea = 2*atan2(sqrt(1+ecc)*sin(ea/2.),sqrt(1-ecc)*cos(ea/2.))

  return ea


def mean_anomaly_2_true_anomaly(ecc,ma,tolerance=1e-14):
  """ Compute the true anomaly from the mean anomaly.
    
  INPUTS
  ------

  ecc: float
    Excentricity
  ma: float
    Mean anomaly

  RETURN
  ------

  nu: float
    True anomaly

  """

  ea = mean_anomaly_2_ecc_anomaly(ecc,ma,tolerance)
  nu = ecc_anomaly_2_true_anomaly(ecc,ea)

  return nu


def flight_path_angle(ecc,ea,unit='radian'):
  """ Compute the flight path angle.

  INPUTS
  ------

  ecc: float
    Eccentricity.
  ea: float
    Eccentric anomaly.
  unit: string
    Unit of the angle.
 
  RETURN
  ------

  fpa: float
    Flight path angle.

  """

  if unit=='radian':
    pass
  elif unit=='degree':
    ea = ea*np.pi/180.
  else:
    print('Bad type of unit')

  if ecc==0:

    fpa = 0.
    
  elif ecc>0 and ecc<1.:

    cosfpa = np.sqrt((1-ecc**2)/(1-ecc**2*np.cos(ea)**2))
    sinfpa = ecc*np.sin(ea)/np.sqrt(1-ecc**2*np.cos(ea)**2) 
    fpa = atan2(sinfpa,cosfpa)

  elif ecc==1:

    nu = ecc_anomaly_2_true_anomaly(ecc,ea)  
    fpa = nu/2.

  elif ecc>1:

    print('Path flight: work in progress...')
    sys.exit()
    cosfpa = np.sqrt((ecc**2-1)/(ecc**2*np.cosh(ea)**2-1))
    sinfpa = -ecc*np.sinh(ea)/np.sqrt(ecc**2*np.cos(ea)**2-1) 
    fpa = atan2(sinfpa,cosfpa)
      
  else:

    print('Error: ecc < 0')

  if unit=='degree':
    fpa = fpa*180./np.pi
    
  return fpa


def dms_2_rad(dms):
  """ Give an angle in radians from an angle in degrees,
  minutes, and seconds.

  INPUTS
  ------

  dms: array of float
    Degrees, minutes, and seconds

  RETURN
  ------

  angle: float
    Angle in radians.

  """

  if dms[0]<0:
    angle = (np.abs(dms[0])+dms[1]/60.+dms[2]/3600.)
    angle = -angle
  else:
    angle = (np.abs(dms[0])+dms[1]/60.+dms[2]/3600.)

  angle = angle*np.pi/180.
    
  return angle


def hms_2_rad(hms):
  """ Give an angle in radians frim an angle in hours,
  minutes, and seconds.

  INPUTS
  ------

  hms: array of float
    Hours, minutes, and seconds

  RETURN
  ------

  angle: float
    Angle in radians.

  """

  angle = 15*(hms[0]+hms[1]/60.+hms[2]/3600.)
  
  return angle


def degree_2_hms(angle):
  """ Give an angle in hours, minutes, and seconds 
  from an angle in degress.
  
  INPUT
  -----

  angle: float
    Angle in degress.

  RETURN
  ------

  hh: float
    Hours.
  mm: float:
    Minutes.
  ss: float
    Seconds.

  """ 

  hh = int(angle*24/360.)
  mm = int((angle-hh*360./24.)/(360./24./60.))
  ss = (angle-hh*360./24.-mm*360./24./60.)/(360./24./60./60.)
  
  return hh,mm,ss


def degree_2_dms(angle):
  """ Give an angle in degress, minutes, and seconds 
  from an angle in degress.
  
  INPUT
  -----

  angle: float
    Angle in degress.

  RETURN
  ------

  dd: float
    Degress.
  mm: float:
    Minutes.
  ss: float
    Seconds.

  """
  
  dd = int(angle)
  mm = int((angle-dd)*60.)
  ss = (angle-dd-mm/60.)*3600.
  
  return dd,mm,ss


def geocentric_2_topocentric(theta_lst,lat_gd,r_geo):
  """ Compute the position from the geocentric frame 
  to the topocentric frame (Vallado, p.162).

  INPUTS
  ------

  theta_lst: float
    Local sideral time [deg]
  lat_gd: float
    Geodedic latitude [deg].
  r_geo: array of float
    Position in the geocentric frame.

  RETURN
  ------

  r_topo: array of float
    Position in the topocentric frame.

  """

  theta_lst = theta_lst*np.pi/180.
  lat_gd = lat_gd*np.pi/180.
    
  rot = rotation(np.pi/2.-lat_gd,2).dot(rotation(theta_lst,3))
  r_topo = rot.dot(r_geo)

  return r_topo


def topocentric_2_geocentric(theta_lst,lat_gd,r_topo):
  """ Compute the position from the topocentric frame 
  to the geocentric frame (Vallado, p.162).

  INPUTS
  ------

  theta_lst: float
    Local sideral time [deg]
  lat_gd: float
    Geodedic latitude [deg].
  r_topo: array of float
    Position in the topocentric frame.

  RETURN
  ------

  r_geo: array of float
    Position in the geocentric frame.

  """

  theta_lst = theta_lst*np.pi/180.
  lat_gd = lat_gd*np.pi/180.
  
  rot = rotation(-theta_lst,3).dot(rotation(-(np.pi/2.-lat_gd),2))
  r_geo = rot.dot(r_topo)

  return r_geo


def latlon_geocentric_2_ecef(phi_gc,lamb,h_ell,angle='rad'):
  """ Give the cartesian coordinate of a station from the 
  geocentric longitude and latitude (Vallado, p.144).

  INPUTS
  ------

  phi_gc: float
    Decimal geocentric latitude  [deg].
  lamb: float
    Decimal longitude [deg].
  h_ell: float
    Ellipsoidal height [m].

  RETURN
  ------

  r: array of float
    Cartesian coordinates in meters.

  """

  if angle=='deg':
    phi_gc = phi_gc*np.pi/180.
    lamb = lamb*np.pi/180. 
  elif angle=='rad':
    pass
  else:
    print('Bad angle unit')
    sys.exit()

  r = np.zeros(3)
  r[0] = np.cos(phi_gc)*np.cos(lamb)
  r[1] = np.cos(phi_gc)*np.sin(lamb)
  r[2] = np.sin(phi_gc)
  
  return r


def latlon_2_ecef(phi_gd,lamb,h_ell,angle='rad'):
  """ Give the cartesian coordinate of a station from the 
  geodedic longitude and latitude (Vallado, p.144).

  INPUTS
  ------

  phi_gd: float
    Decimal geodetic latitude [deg].
  lamb: float
    Decimal longitude [deg].
  h_ell: float
    Ellipsoidal height [m].

  RETURN
  ------

  r: array of float
    Cartesian coordinates in m and m/s.

  """

  if angle=='deg':
    phi_gd = phi_gd*np.pi/180.
    lamb = lamb*np.pi/180. 
  elif angle=='rad':
    pass
  else:
    print('Bad angle unit')
    sys.exit()
      
  #earth_eq_radius = 6378.137E3#63E3
  #e_earth = np.sqrt(0.006694385)

  c_earth = earth_eq_radius/np.sqrt(1-e_earth**2*np.sin(phi_gd)**2)
  s_earth = c_earth*(1-e_earth**2)
  #print(c_earth)
  #print(s_earth)

  r = np.zeros(6)
  r[0] = (c_earth+h_ell)*np.cos(phi_gd)*np.cos(lamb)
  r[1] = (c_earth+h_ell)*np.cos(phi_gd)*np.sin(lamb)
  r[2] = (s_earth+h_ell)*np.sin(phi_gd)
  
  return r


def ecef_2_latlon(r_ecef):
  """ Convertion from cartesian coordinates in the ECEF frame to
  longitude and latitude. Reference : Algorithm 12, p.172 in Vallado 2012.
    
  INPUTS
  ------

  r_ecef: array
    Cartisian coordinates in the ECEF frame [meters].

  RETURN
  ------

  lon: float
    Longitude [radian]
  lat: latitude 
    Geodetic latitude [radian]

  """

  e_earth = np.sqrt(0.006694385)
    
  r = np.sqrt(r_ecef[0]**2+r_ecef[1]**2+r_ecef[2]**2)
  r_delta = np.sqrt(r_ecef[0]**2+r_ecef[1]**2)
  
  sin_alpha = r_ecef[1]/r_delta
  cos_alpha = r_ecef[0]/r_delta
  tan_alpha = sin_alpha/cos_alpha
  alpha     = atan(tan_alpha)
  alpha     = atan2(r_ecef[1],r_ecef[0])

  delta = asin(r_ecef[2]/r)
  #print 'Alpha:',alpha*180./np.pi
  #print 'Delta:',delta*180./np.pi

  phi_gd = delta
  counter = 0
  phi_gd_old = 1E10
  c_earth = earth_eq_radius/np.sqrt(1-e_earth**2*np.sin(phi_gd)**2)

  while np.abs(phi_gd-phi_gd_old)>5.E-10:
    phi_gd_old = phi_gd
    tanphi_gd = (r_ecef[2]+c_earth*e_earth**2*np.sin(phi_gd))/r_delta
    phi_gd    = atan(tanphi_gd)
    counter += 1
    if counter>10:
      break

  h_ell = r_delta/np.cos(phi_gd)-c_earth
  if phi_gd*180./np.pi>89:
    s_earth = earth_eq_radius*(1-e_earth**2)/np.sqrt(1-e_earth**2*np.sin(phi_gd)**2) 
    h_ell = r_ecef[2]/np.sin(phi_gd)-s_earth
  
  lamb   = alpha   #Geodetic longitude
  phi_gd = delta#phi_gd  #Geodetic latitude

  return lamb,phi_gd,h_ell


def rotation(alpha,axe):
  """ Give the matrix rotation (Vallado p.162).
 
  INPUT
  -----
 
  alpha: float 
    Angle of rotation.
  axe: integer
    Axe of rotation.

  RETURN
  ------

  rot: matrix of float [3x3]
    Matrix rotation.

  """

  rot = np.zeros((3,3))   
  if axe==1:
    rot[0,0] = 1
    rot[1,1] = np.cos(alpha)
    rot[1,2] = np.sin(alpha)
    rot[2,1] = -np.sin(alpha)
    rot[2,2] = np.cos(alpha)
  elif axe==2:
    rot[0,0] = np.cos(alpha)
    rot[0,2] = -np.sin(alpha)
    rot[1,1] = 1
    rot[2,0] = np.sin(alpha)
    rot[2,2] = np.cos(alpha)
  elif axe==3:
    rot[0,0] = np.cos(alpha)
    rot[0,1] = np.sin(alpha)
    rot[1,0] = -np.sin(alpha)
    rot[1,1] = np.cos(alpha)
    rot[2,2] = 1
  else:
    print('Error: bad choice of the rotation axis.')
    sys.exit()

  return rot 

    
def convert_ephemeris_2_snapshot(list_ephemeris):
  """ Convert a list of ephemeris to a list of snapshots.
  
  INPUT
  -----

  list_ephemeris: list of dataframes
    List of ephemeris.
  
  RETURN
  ------

  list_snapshots: list of dataframes
    List of snapshots.

  """

  list_snapshots = []

  nb = 0
  for item in list_ephemeris:
    if len(item)>nb:
      nb = len(item)

  for i in range(0,nb,1):
    list_coord = []
    for ephemeris in list_ephemeris:
      if i<len(ephemeris):
        list_coord.append(ephemeris[i:i+1])
        date = ephemeris[i:i+1]['Date'].iloc[0]
    snapshot = pd.concat(list_coord,ignore_index=True)
    list_snapshots.append([date,snapshot])                          
    
  return list_snapshots


def coord_interpol(coord1,coord2,var):
  """ Perform an interpolation.
        
  INPUTS
  ------
        
  coord1: dataframe
    Contains the first set of observations.
  coord2: dataframe
    Contains the second set of observations (reference).
  var: list
    List of the variables to compare.
        
  RETURN
  ------
        
  new_coord: dataframe
    New set of coordinates interpolated on the first set.
  
  Condition:
    /ti/================================/tf/
      /ri_ref/==================/tf_ref/
      
  """

  coord1 = coord1.reset_index()
  coord2 = coord2.reset_index()
 
  ti = coord1["Date"].iloc[0]
  tf = coord1["Date"].iloc[len(coord1)-1]
  ti_ref = coord2["Date"].iloc[0]
  tf_ref = coord2["Date"].iloc[len(coord2)-1]
  
  new_coord = copy.deepcopy(coord2["Date"]).reset_index()
  
  jd1 = [date_2_jd(row) for row in coord1["Date"]]
  jd2 = [date_2_jd(row) for row in coord2["Date"]]
  jd1 = np.asarray(jd1)
  jd2 = np.asarray(jd2)  

  jdi = jd1[0]
  jdf = jd1[len(jd1)-1]
  jdi_ref = jd2[0]
  jdf_ref = jd2[len(jd2)-1]  
 
  if ((jdi_ref<jdi) or (jdf_ref>jdf)):
    print('(interpolation) Error with the bounds of the data')
    print('Cal = [',jdi,jdf,']')
    print('Ref = [',jdi_ref,jdf_ref,']')
    print(jdi_ref,'<=',jdi,(jdi_ref<jdi))
    print(jdf_ref,'>=',jdf,(jdf_ref>jdf))
    sys.exit()
    
  for item in var:
    var_obs1 = np.asarray(coord1[item].values.tolist())
    var_obs2 = np.asarray(coord2[item].values.tolist())
    f = interp1d(jd1,var_obs1,kind='linear')
    var_obs1 = f(jd2)
    var_obs1 = pd.DataFrame(var_obs1,columns=[item])
    new_coord = pd.concat([new_coord,var_obs1],axis=1)
  
  return new_coord


def ntw_frame(cart):
  """ Give the matrix to pass from cartesian to the NTW frame.

  INPUTS
  ------

  cart: float array
    Cartesian coordinates (r,v).
  mu: float

  RETURN
  ------

  ntw: matrix
    Operator to change of basis vector.

  """

  t = cart[3:6]/np.sqrt(np.dot(cart[3:6],cart[3:6]))
  w = np.cross(cart[0:3],cart[3:6])
  w = w[0:3]/np.sqrt(np.dot(w[0:3],w[0:3]))
  n = np.cross(cart[3:6],w[0:3])
  n = n[0:3]/np.sqrt(np.dot(n[0:3],n[0:3]))
  
  ntw = np.zeros((3,3))
  ntw[0,0:3] = n[0:3]
  ntw[1,0:3] = t[0:3] 
  ntw[2,0:3] = w[0:3]
  
  return ntw
