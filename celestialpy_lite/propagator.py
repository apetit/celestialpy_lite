#!/usr/bin/python
# Licensed under a 3-clause BSD style license - see LICENSE.rst
# -*-coding:Utf-8 -*

"""
  propagator.py
  Author: Alexis Petit, Post-doc, IMCCE, IFAC-CNR
  Date: 2019 02 12
  This script is a part of CelestialPy. It allows to work
  with several orbit propagators:
  - Propagator IFAC (E. M. Alessi, IFAC)
  - GEO model (D. Casanova, UNamur)
  - NIMASTEP/SYMPLEC 8.0 (UNamur)
  - Stela_v3.2 (java version) (CNES)
  - SATlight/Fast/Copine 4.0 (IMCCE)
  - Thalassa
  The script contains a propagator wrapper and a module 
  performing a propagation of one or a set of initial 
  conditions given in a dataframe.
"""


#Packages


import numpy as np
import pandas as pd
import sys
import os
import time
import progressbar
import configparser
from xml.dom import minidom
from datetime import datetime,timedelta


#Internal packages


from celestialpy_lite.time_conversion import jd_2_date
from celestialpy_lite.time_conversion import date_2_decimal_year
from celestialpy_lite.time_conversion import date_2_jd
from celestialpy_lite.time_conversion import jd_2_jd_cnes
from celestialpy_lite.constants import earth_eq_radius


#Code


__all__ = [
  'propagate',
  'Propagator_wrapper'
]


def propagate(work_dir,path_op,population,param,burnlist={},para=False,verbosity=True,namedir=''):
  """ Propagate a set of initial conditions.

  INPUT
  -----

  work_dir: string
    Name of the working directory.
  path_op: string
    Propagator directory.
  population: dataframe
    Set of initial conditions.
  param: string
    File containing the paramaters of the orbit propagator.

  OPTIONAL INPUTS
  ---------------

  burnlist: dictionary
    Burn list for manoeuvres.
  para: logical
    Flag for a parallel mode.
  verbosity: boolean
    Say if we print informations.  

  RETURN
  ------

  list_ephemeris: list of dataframes
    List of ephemeris.

  """

  if namedir=='':
    namedir = work_dir+'/op_temp'
  else:
    namedir = work_dir+'/'+namedir
    
  list_ephemeris = []
 
  t_i = time.time()

  if burnlist:
    my_propagator = Propagator_wrapper(param,burnlist=burnlist)
  else:
    my_propagator = Propagator_wrapper(param) 

  my_propagator.prop_dir = path_op 
    
  if len(population)>1:
    
    if para==True:
    
      if verbosity==True:    
        print('Propagation (parallel mode)')

      list_proc = []
      for i in range(0,len(population),1):
        if burnlist:
          my_propagator = Propagator_wrapper(param,burnlist=burnlist)
        else:
          my_propagator = Propagator_wrapper(param)
        namedir = namedir+'_'+str(i)  
        p = Propagator_th(my_propagator,population[i:i+1],namedir)
        list_proc.append(p)

      for p in list_proc:
        p.start()
      for p in list_proc:
        p.join()
        ephemeris = p.get()
        list_ephemeris.append(ephemeris)

    else:

      if verbosity==True:
        print('Propagation (serie mode)')
      #bar = progressbar.ProgressBar(max_value=100)#len(population))
      for i in range(0,len(population),1):   
        ephemeris = my_propagator.go(population[i:i+1],namedir)
        ephemeris['BC[m2/kg]'] = population[i:i+1]['BC[m2/kg]'].iloc[0]
        ephemeris['Id Norad'] = population[i:i+1]['Id Norad'].iloc[0]
        ephemeris['Name'] = population[i:i+1]['Name'].iloc[0]
        list_ephemeris.append(ephemeris)
        #if verbosity==True:
        #  bar.update(i+1)

  else:

    ephemeris = my_propagator.go(population[0:1],namedir)
    ephemeris['BC[m2/kg]'] = population[0:1]['BC[m2/kg]'].iloc[0]
    ephemeris['Id Norad'] = population[0:1]['Id Norad'].iloc[0]
    ephemeris['Name'] = population[0:1]['Name'].iloc[0]
    list_ephemeris.append(ephemeris) 
    
  t_f = time.time() 

  tot_time = t_f-t_i
  if verbosity==True:    
    print('Time = ',tot_time)
  
  return list_ephemeris


class Propagator_wrapper():
  """ A class allowing to use different orbit propagators.
  """
  
  def __init__(self,param='',burnlist={}):
    """ Initialization

    INPUT
    -----

    param: string
  
    """

    self.propagator_directory = None
    self.id_number = 1
    self.name_output = 'computation'
    self.burnlist=burnlist
    self.prop_dir = ''

    if param=='':
      
      # General
      self.op =  'nimastep'
      
      # Inputs
      self.inputs = {}
      self.inputs['initial_coordinates_type'] = ['kepler','osc','deg']
      self.inputs['number_satellite'] = 1
      self.inputs['Cd'] = 2.2
    
      # Outputs
      self.outputs = {}
      self.outputs['ephem_write'] = 1
      self.outputs['output_step_day'] = 0 
      self.outputs['output_step_sec'] = 3600
      self.outputs['coordinates_type'] = ['kepler','osc','deg']
      self.outputs['short_periods'] = 1
      self.outputs['short_periods_suppr'] = 0
      self.outputs['chaos_indicator'] = 0
      self.outputs['write_atmosphere'] = 0
      self.outputs['clean'] = 1
    
      # Parameter of the integration
      self.integration = {}
      self.integration['time_day'] = 10
      self.integration['time_sec'] = 0
      self.integration['step_day'] = 0
      self.integration['step_sec'] = 60
      self.integration['type'] = 'ABM10'
      self.integration['fast_type'] = 'A'
    
      # Forces (geopotential)
      self.forces = {}
      self.forces['degree_max'] = 0
      self.forces['order_max'] = 0
      self.forces['harmonics'] = np.zeros((5,11))
      self.forces['harmonics'][0,0]=1
      self.forces['harmonics'][0,2]=1
    
      # Forces (third body)
      self.forces['sun'] = 1
      self.forces['moon'] = 1
      self.forces['ephemeris_3c'] = 'Elsana'

      # Forces (drag)
      self.forces['drag'] = 0
      self.forces['space_weather'] = 2
      self.forces['f107'] = 140
      self.forces['ap'] = 15    
      self.forces['drag_coefficient'] = 2.2
      self.forces['pts_quadrature_fr'] = 33
      self.forces['fourier_rang_max_fr'] = 10  
    
      # Forces (solar radiation pressure)
      self.forces['srp'] = 0
      self.forces['srp_method'] = 'analytique'
      self.forces['albedo'] = 1
      self.forces['umbra_model'] = 'Cylinder'
      self.forces['pts_quadrature_srp'] = 33
      self.forces['fourier_rang_max_prs'] = 10

      # Manoeuvers
      self.forces['station_keeping'] = 1

    else:
      
      self.load_parameters(param)


  def load_parameters(self,name_file):
    """ Load the parameters from a configuration file.
   
    INPUT 
    -----

    name_file: string
      Name of the configuration file.

    """
  
    if os.path.isfile(name_file)==False:
      print('Parameter file does not exist : '+name_file)
      sys.exit()
    
    cfg = configparser.ConfigParser()   
    try:
      cfg.read(name_file)
    except IOError: 
      print("Error: config file does not appear to exist.")
      sys.exit()

    #print('Load integration parameters from '+name_file)
      
    # Propagator
    self.op = cfg.get('General','name')      

    # Inputs
    self.inputs = {}
    self.inputs['number_satellite'] = cfg.get('Inputs','number_satellite')
    self.inputs['initial_coordinates_type'] = ['kepler','osc','deg']
    self.inputs['initial_coordinates_type'][0] = cfg.get('Inputs','coordinate')
    self.inputs['initial_coordinates_type'][1] = cfg.get('Inputs','type')
    self.inputs['initial_coordinates_type'][2] = cfg.get('Inputs','angle')     
    self.inputs['Cd'] = cfg.get('Inputs','Cd')
      
    # Outputs
    self.outputs = {}
    self.outputs['ephem_write'] = cfg.get('Outputs','ephem_write')
    self.outputs['output_step_day'] = cfg.get('Outputs','output_step_day')
    self.outputs['output_step_sec'] = cfg.get('Outputs','output_step_sec')
    self.outputs['coordinates_type'] = ['kepler','osc','deg']
    self.outputs['coordinates_type'][0] = cfg.get('Outputs','coordinate')   
    self.outputs['coordinates_type'][1] = cfg.get('Outputs','type')
    self.outputs['coordinates_type'][2] = cfg.get('Outputs','angle')    
    self.outputs['short_periods'] = cfg.get('Outputs','short_periods')
    self.outputs['short_periods_suppr'] = cfg.get('Outputs','short_periods_suppr')
    self.outputs['chaos_indicator'] = cfg.get('Outputs','chaos_indicator')
    self.outputs['write_atmosphere'] = cfg.get('Outputs','write_atmosphere')
    self.outputs['clean'] = cfg.get('Outputs','clean')

    # Parameter of the integration
    self.integration = {}
    self.integration['time_day'] = cfg.get('Integration','time_day')
    self.integration['time_sec'] = cfg.get('Integration','time_sec')
    self.integration['step_day'] = cfg.get('Integration','step_day')
    self.integration['step_sec'] = cfg.get('Integration','step_sec')    
    self.integration['type'] = cfg.get('Integration','type')
    if cfg.get('General','name') == 'copine':
      self.integration['fast_type'] = 'N'
    elif cfg.get('General','name') == 'satlight':
      self.integration['fast_type'] = 'S'    
    elif cfg.get('General','name') == 'fast':
      self.integration['fast_type'] = 'A'
    elif cfg.get('General','name') == 'nimastep':
      pass      
    elif cfg.get('General','name') == 'ifac':
      pass
    elif cfg.get('General','name') == 'geo_model':
      pass
    elif cfg.get('General','name') == 'stela':
      pass
    elif cfg.get('General','name') == 'thalassa':
      pass                  
    else:
      print('Bad method of integration')
      sys.exit()

    # Forces (geopotential)
    self.forces = {}
    self.forces['degree_max'] = cfg.get('Geopotential','degree')
    self.forces['order_max'] = cfg.get('Geopotential','order')
    self.forces['harmonics'] = np.zeros((5,11))
    self.forces['harmonics'][0,0] = 1
    self.forces['harmonics'][1,0] = cfg.get('Geopotential','C2-')
    self.forces['harmonics'][2,0] = cfg.get('Geopotential','C3-')
    self.forces['harmonics'][3,0] = cfg.get('Geopotential','C4-')
    self.forces['harmonics'][4,0] = cfg.get('Geopotential','C5-')
    
    # Forces (third body)
    self.forces['sun'] = cfg.get('Third body','sun')
    self.forces['moon'] = cfg.get('Third body','moon')
    self.forces['ephemeris_3c'] = cfg.get('Third body','model_3b')

    # Forces (drag)
    self.forces['drag'] = cfg.get('Drag','drag')
    self.forces['space_weather'] = cfg.get('Drag','space_weather')
    self.forces['f107'] = cfg.get('Drag','f107')
    self.forces['ap'] = cfg.get('Drag','ap')
    self.forces['drag_coefficient'] = cfg.get('Drag','Cd')
    self.forces['pts_quadrature_fr'] = cfg.get('Drag','pts_quadrature_fr')
    self.forces['fourier_rang_max_fr'] = cfg.get('Drag','fourier_rang_max_fr')
   
    # Forces (solar radiation pressure)
    self.forces['srp'] = cfg.get('Solar radiation pressure','srp')
    self.forces['srp_method'] = cfg.get('Solar radiation pressure','srp_method')
    self.forces['albedo'] = cfg.get('Solar radiation pressure','albedo')
    self.forces['umbra_model'] = cfg.get('Solar radiation pressure','umbra_model')
    self.forces['pts_quadrature_srp'] = cfg.get('Solar radiation pressure','pts_quadrature_srp')
    self.forces['fourier_rang_max_prs'] = cfg.get('Solar radiation pressure','fourier_rang_max_prs')

    # Manoeuvers

    if cfg.get('Manoeuvres','activate')=='yes':
      self.forces['maoeuvres'] = 1
    else:
      self.forces['maoeuvres'] = 0

    if cfg.get('Orbit raising','activate')=='yes':
      self.forces['orbit_raising']=1
      self.forces['orbit']=float(cfg.get('Orbit raising','orbit'))*1E3+earth_eq_radius      
      self.forces['thrust']=float(cfg.get('Orbit raising','thrust'))
    else:
      self.forces['orbit_raising']=0      

    if cfg.get('Station keeping','activate')=='yes':
      self.forces['station_keeping']=1
      self.forces['tolerance']=float(cfg.get('Station keeping','tolerance'))     
    else:
      self.forces['station_keeping']=0
      
    if cfg.get('Decommissionning','activate')=='yes':
      self.forces['decommissionning']=1
      self.forces['thrust']=float(cfg.get('Decommissionning','thrust'))
      self.forces['orbit_decommissionning']=[ float(item) for item in cfg.get('Decommissionning','orbit').split('x')]
    else:
      self.forces['decommissionning']=0

      
  def get_parameters(self):
    """ Return a dictionary of the input.

    RETURN
    ------
  
    inputs: dictionary
      Parameters about the inputs
    outputs: dictionary
      Parameters about the outputs
    integration: dictionary
      Parameters about the integration
    forces: dictionary    
      Parameters about the forces    

    """

    return self.inputs,self.outputs,self.integration,self.forces

  
  def set_parameters(self,inputs,outputs,integration,forces):
    """ Update the parameters used for the propagations.
     
    INPUTS
    ------
  
    inputs: dictionary
      Parameters about the inputs
    outputs: dictionary
      Parameters about the outputs
    integration: dictionary
      Parameters about the integration
    forces: dictionary    
      Parameters about the forces

    """
    
    self.inputs = inputs   
    self.outputs = outputs
    self.forces = forces
    self.integration = integration


  def go(self,state_vector,work_dir):
    """ Orbit propagation.

    INPUTS
    ------

    state_vector: dataframe
      Initial conditions (keplerian or cartesian elements).
    work_dir: string
      Name of the working directory.

    RETURN
    ------

    ephemeris: dataframe
      Ephemeris of the orbit.

    """

    self.set_parameters(self.inputs,self.outputs,self.integration,self.forces)
        
    if self.op=='nimastep':
      ephemeris = self.nimastep(state_vector,work_dir)
    elif self.op=='fast':
      ephemeris = self.copain(state_vector,output=work_dir)
    elif self.op=='satlight':
      ephemeris = self.copain(state_vector,output=work_dir)
    elif self.op=='copine':
      ephemeris = self.copain(state_vector,output=work_dir)
    elif self.op=='ifac':
      ephemeris = self.ifac_propagator(state_vector,output=work_dir)
    elif self.op=='geo_model':
      ephemeris = self.geo_propagator(state_vector,output=work_dir)
    elif self.op=='stela':
      ephemeris = self.stela(state_vector,output=work_dir)
    elif self.op=='thalassa':
      ephemeris = self.thalassa(state_vector,output=work_dir)
      
    return ephemeris

  
  ########################################################################
  #
  # WRAPPER GEO Model 
  #
  ########################################################################
     
  def geo_propagator(self,state_vector,output='geo_prop'):
    """ Call the GEO model.

    INPUTS
    ------

    work_dir: string
      Working directory.
    state_vector: dataframe
      Initial conditions.
    output: string
      Output directory.

    RESULTS
    -------

    ephemeris: dataframe
      Ephemeris computed.

    """

    #self.prop_dir = DB_env().get_cfg().get('Softwares','geo_model')
    self.name_output = output
    
    if (os.path.exists(self.name_output)):
      os.system('rm -r '+self.name_output)
    os.system('mkdir '+self.name_output)
    os.system('cp -r '+self.prop_dir+'/* ./'+self.name_output+'/')

    self.modify_geo_model_parser(state_vector)
    ephemeris = self.propagation_with_geo_model()
    ephemeris['JD'] = date_2_jd(state_vector['Date'].iloc[0])+ephemeris['Time[day]']
    ephemeris['Date'] = ephemeris['JD'].apply(lambda x: jd_2_date(float(x)))
    ephemeris['BC[m2/kg]'] = state_vector.iloc[0]['BC[m2/kg]']
    
    if int(self.outputs['clean'])==1:
      os.system('rm -r ./'+self.name_output)
    
    return ephemeris


  def modify_geo_model_parser(self,state_vector):
    """ Modify parser of the GEO model.

    INPUT
    -----

    state_vector: dateframe
      Initial conditions.

    """

    f = open(self.name_output+'/initial_conditions.dat','w')

    # Initial conditions

    idsat  = state_vector.iloc[0]['Id Norad']
    date   = state_vector.iloc[0]['Date']
    year   = date.strftime('%Y')
    month  = date.strftime('%m')
    day    = date.strftime('%d')
    hour   = date.strftime('%H')
    minu   = date.strftime('%M')
    sec    = date.strftime('%S')    
    jd0    = date_2_jd(date)
    sma0   = state_vector.iloc[0]['a[m]']/1000.
    ecc0   = state_vector.iloc[0]['e']
    inc0   = state_vector.iloc[0]['i[deg]']*np.pi/180.
    raan0  = state_vector.iloc[0]['RAAN[deg]']*np.pi/180.
    omega0 = state_vector.iloc[0]['Omega[deg]']*np.pi/180.    
    ma0    = state_vector.iloc[0]['MA[deg]']*np.pi/180.
    am0    = state_vector.iloc[0]['BC[m2/kg]']/2.2
    
    f.write('{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} {12} {13}\n'.format(idsat,sma0,ecc0,inc0,raan0,omega0,ma0,year,month,day,hour,minu,sec,am0))

    # Integration parameters
    
    #tspan_day = float(self.integration['time_day'])+float(self.integration['time_sec'])/86400.
    #tspan = tspan_day/365.25
    #tstep = float(self.integration['step_day'])+float(self.integration['step_sec'])/86400.
    
    f.close()


  def propagation_with_geo_model(self):
    """ Compute the orbit with the GEO model.

    RETURN
    ------

    ephemeris: dataframe
      Contain the result of the propagation.

    """

    # Propagation
    os.system('cd {0}; ./prop_geo > prop.log'.format(self.name_output))

    # Read ephemeris
    ephemeris = []
    f = open(self.name_output+'/output/ephem.out','r')
    d = f.readlines()
    f.close()
    
    for row in d:
      row = row.split()
      time = float(row[0])*365.25
      sma = np.nan
      ecc = float(row[1])
      inc = float(row[2])*180/np.pi      
      raan = np.nan
      omega = np.nan
      ma = np.nan
      ephemeris.append([time,sma,ecc,inc,raan,omega,ma])
    headers = ['Time[day]','a[m]','e','i[deg]','RAAN[deg]','Omega[deg]','MA[deg]']
    ephemeris = pd.DataFrame(ephemeris,columns=headers)

    return ephemeris
  
    
  ########################################################################
  #
  # WRAPPER IFAC 
  #
  ########################################################################
     
  def ifac_propagator(self,state_vector,output='ifac_prop'):
    """ Call the IFAC orbit propagator.

    INPUTS
    ------

    work_dir: string
      Working directory.
    state_vector: dataframe
      Initial conditions.

    RESULTS
    -------

    ephemeris: dataframe
      Ephemeris computed.

    """
    
    #self.prop_dir = DB_env().get_cfg().get('Softwares','ifacprop')
    self.name_output = output
    
    if (os.path.exists(self.name_output)):
      os.system('rm -r '+self.name_output)
    os.system('mkdir '+self.name_output)
    os.system('mkdir '+self.name_output+'/results')
    os.system('cp -r '+self.prop_dir+'/bin/* ./'+self.name_output+'/')
    
    self.modify_ifac_parser(state_vector)
    ephemeris = self.propagation_with_ifac_prop()
    ephemeris['BC[m2/kg]'] = state_vector.iloc[0]['BC[m2/kg]']
    
    if int(self.outputs['clean'])==1:
      os.system('rm -r ./'+self.name_output)
    
    return ephemeris    


  def modify_ifac_parser(self,state_vector):
    """ Modify parser of the IFAC propagator.

    INPUT
    -----

    state_vector: dateframe
      Initial conditions.

    """

    f = open(self.name_output+'/initial_conditions.txt','w')

    # Initial conditions
    
    date   = state_vector.iloc[0]['Date']
    jd0    = date_2_jd(date)
    sma0   = state_vector.iloc[0]['a[m]']/1000.
    ecc0   = state_vector.iloc[0]['e']
    inc0   = state_vector.iloc[0]['i[deg]']
    raan0  = state_vector.iloc[0]['RAAN[deg]']
    omega0 = state_vector.iloc[0]['Omega[deg]']    
    ma0    = state_vector.iloc[0]['MA[deg]']

    f.write('{0} {1} {2} {3} {4} {5}\n'.format(sma0,ecc0,inc0,raan0,omega0,ma0))

    # Integration parameters
    tspan_day = float(self.integration['time_day'])+float(self.integration['time_sec'])/86400.
    tspan = tspan_day/365.25
    dt_day  = self.outputs['output_step_day']
    dt_sec  = self.outputs['output_step_sec']
    tstep = float(dt_day)+float(dt_sec)/86400.
    f.write('{0:4} {1:4} {2:10}\n'.format(tspan,tstep,jd0))

    # Physical parameter

    am0 = state_vector.iloc[0]['BC[m2/kg]']/float(self.forces['drag_coefficient'])
    am0 = am0*1E-6
    
    f.write('{0}\n'.format(am0))

    # Physical model

    forces = ' '    
    for i in range(1,5,1):
      if (i<=int(self.forces['degree_max'])):
        forces += '1 '
      else:
        forces += '0 '
    if (int(self.forces['sun'])==1):
      forces += '1 '
    else:
      forces += '0 '
    if (int(self.forces['moon'])==1):
      forces += '1 '
    else:
      forces += '0 '
    forces += '1 '
    forces += '1 '
    f.write('{0}\n'.format(forces))
      
    # Station keeping
    
    if int(self.forces['station_keeping'])==1:
      f.write('{0}\n'.format(self.forces['tolerance']))
    else:
      f.write('0\n')

    # Orbit raising
    
    if int(self.forces['orbit_raising'])==1:
      thrust = self.forces['thrust']
      a_f = self.forces['orbit']
      f.write('{0} {1}\n'.format(thrust/1000.,a_f/1000.)) #thrust in km/s2
    else:
      f.write('0 0 ! orbit raising (thurst,a_f) \n')

    # Decommissionning
    
    if int(self.forces['decommissionning'])==1:
      f.write('{0} {1} {2} ! decommissionning\n'.format(self.forces['thrust']/1000.,self.forces['orbit_decommissionning'][0],self.forces['orbit_decommissionning'][1]))
    else:
      f.write('0 0 0 ! decommissionning (thurst,apo.[km],per.[km])\n')      
      
    f.close()     
     

  def propagation_with_ifac_prop(self):
    """ Compute the orbit with the IFAC propagator.

    RETURN
    ------

    ephemeris: dataframe
      Contain the result of the propagation.

    """

    # Propagation
    if True:
      os.system('cd {0}; ./prop_leo'.format(self.name_output))
    else:
      os.system('cd {0}; ./prop_leo > prop.log'.format(self.name_output))
      
    # Read ephemeris
    ephemeris = []
    f = open(self.name_output+'/ephem.out','r')
    d = f.readlines()
    f.close()

    #k = 0
    for row in d[1:]:
      row = row.split()
      mjd = float(row[0])
      #k += 1
      #if k>10:
      #  sys.exit()
      jd = mjd + 2400000.5
      sma = float(row[1])*1000.
      ecc = float(row[2])
      inc = float(row[3])      
      raan = float(row[4])
      omega = float(row[5])
      ma = float(row[6])
      #dv = float(row[8])
      ephemeris.append([jd,mjd,sma,ecc,inc,raan,omega,ma])
    headers = ['JD','MJD','a[m]','e','i[deg]','RAAN[deg]','Omega[deg]','MA[deg]']
    ephemeris = pd.DataFrame(ephemeris,columns=headers)
    ephemeris['Date'] = ephemeris['JD'].apply(lambda x: jd_2_date(float(x)))

    return ephemeris

  
  ########################################################################
  #
  # WRAPPER NIMASTEP 8.0 
  #
  ########################################################################    
   
  def nimastep(self,state_vector,output):
    """ Compute an orbit with NIMASTEP.

    INPUTS
    ------
    
    state_vector: dataframe
      Initial conditions (keplerian or cartesian elements).
    output: string
      Working directory
    
    RETURN
    ------
 
    ephemeris: dataframes
      The evolution of the initial state vector.

    """

    #self.prop_dir = DB_env().get_cfg().get('Softwares','nimastep')
    self.name_output = output
    
    if (os.path.exists(self.name_output)):
      os.system('rm -r '+self.name_output)
    os.system('mkdir '+self.name_output)
    os.system('mkdir '+self.name_output+'/results')
    os.system('cp -r '+self.prop_dir+'/bin/* ./'+self.name_output+'/')

    self.modify_ic_file(state_vector)
    self.modify_parser()
    if self.burnlist:
      self.modif_burnlist(self.burnlist)
    ephemeris = self.propagation_with_nimastep()
    
    if int(self.outputs['clean'])==1:
      os.system('rm -r ./'+self.name_output)
    
    return ephemeris    

  
  def modify_ic_file(self,state_vector):
    """ Modify the file containing the initial conditions for NIMASTEP.

    INPUTS
    ------
    
    state_vector: dataframe
      Initial conditions (keplerian or cartesian elements).

    """

    self.id_number = state_vector.iloc[0]["Id Norad"]
    
    f = open(self.name_output+'/input/ic_file.txt','w')

    if (self.inputs['initial_coordinates_type'][0]=='kepler'):
      alt = '%010.4f' % float((state_vector.iloc[0]["a[m]"]-earth_eq_radius)/1000.)
      q1  = '%013.4f' % float(state_vector.iloc[0]["a[m]"])
      q2  = '%7.6f'   % float(state_vector.iloc[0]["e"])
      q3  = '%08.4f'  % float(state_vector.iloc[0]["i[deg]"])
      p1  = '%08.4f'  % float(state_vector.iloc[0]["RAAN[deg]"])
      p2  = '%08.4f'  % float(state_vector.iloc[0]["Omega[deg]"])
      p3  = '%08.4f'  % float(state_vector.iloc[0]["MA[deg]"])      
      f.write('{0:15} {1:10} {2} {3:25} {4} {5} {6} {7} {8} {9}\n'.format('Time[Jd]','ID NORAD','Name','a[m]','e','i[deg]','RAAN[deg]','Per[deg]','MA[deg]','BC[m2/kg]'))
      
    elif (self.inputs['initial_coordinates_type'][0]=='cartesian'):
      q1  = '%014.6f' % float(state_vector.iloc[0]["x[m]"])
      q2  = '%014.6f' % float(state_vector.iloc[0]["y[m]"])
      q3   = '%014.6f' % float(state_vector.iloc[0]["z[m]"])
      p1  = '%014.6f' % float(state_vector.iloc[0]["vx[m/s]"])
      p2  = '%014.6f' % float(state_vector.iloc[0]["vy[m/s]"])
      p3  = '%014.6f' % float(state_vector.iloc[0]["vz[m/s]"])
      f.write('{0:15} {1:10} {2} {3:25} {4} {5} {6} {7} {8} {9}\n'.format('       Time[Jd]','ID NORAD','Name','x[m]','y[m]','z[m]','Vx[m/s]','Vy[m/s]','Vz[m/s]','BC[m2/kg]'))
    else:
      print('Error: bad coordinate type')
      sys.exit()
      
    date     = state_vector.iloc[0]["Date"]  
    
    time_yr  = '%10.4f'  % date_2_decimal_year(date)
    time_jd  = '%20.12f' % date_2_jd(date)
    id_norad = '%8d'     % int(state_vector.iloc[0]["Id Norad"])
    name     = state_vector.iloc[0]["Name"].replace(' ','_').replace('/','').replace('\\','')
    bc       = '%09.6f'  % float(state_vector.iloc[0]["BC[m2/kg]"])        
    
    f.write('{0} {1} {2:25} {3} {4} {5} {6} {7} {8} {9}\n'.format(str(time_jd),str(id_norad),str(name),str(q1),str(q2),str(q3),str(p1),str(p2),str(p3),str(bc)))
    f.close()
    

  def modif_burnlist(self,burnlist):
    """ Modify the file of the burn list.

    INPUTS
    ------

    burnlist: list of dictionaries
      Burn list

    """

    f = open(self.name_output+'/data/manoeuvres/manoeuvres.txt','w')
    f.write('{0}\n'.format(len(burnlist)))
    
    for param in burnlist:
      if param['type']=='burn':
        jd    = int(param['jd'])
        sec   = param['sec']
        frame = param['frame']
        x = param['orientation'][0]
        y = param['orientation'][1]
        z = param['orientation'][2]
        thrust   = param['thrust']
        duration = param['duration']
        f.write('{0}\n'.format(param['type']))
        f.write('{0} {1} {2} {3} {4} {5} {6} {7}\n'.format(jd,sec,frame,x,y,z,thrust,duration))
      if param['type']=='decommissioning':
        jd    = int(param['jd'])
        sec   = param['sec']
        thrust  = param['thrust']
        perigee = param['perigee']
        method  = param['method'] 
        f.write('{0}\n'.format(param['type']))
        f.write('{0} {1} {2} {3} {4}\n'.format(jd,sec,thrust,perigee,method))
      elif param['type']=='stationkeeping':
        jd    = int(param['jd'])
        sec   = param['sec']
        thrust  = param['thrust']
        altitude = param['altitude']
        tolerance = param['tolerance']
        duration = param['duration']
        f.write('{0}\n'.format(param['type']))
        f.write('{0} {1} {2} {3} {4} {5}\n'.format(jd,sec,thrust,altitude,tolerance,duration))

    f.close()
    

  def modify_parser(self):
    """ Modify the file containing the parameters for the propagation with NIMASTEP.
    """
    
    # Inputs
    self.call_parser('total_number_satellites','  total_number_satellites='+str(self.inputs['number_satellite'])+'\n')   
    self.call_parser('input_file_name'        ,'  input_file_name="ic_file"\n')

    if 'kepler' in self.inputs['initial_coordinates_type'][0]:
      flag = 'kepl_'
    elif 'cartesian' in self.inputs['initial_coordinates_type'][0]:
      flag = 'cart_'
    else:
      print('Error with the type of initial conditions')
      print(self.inputs['initial_coordinates_type'][0])
      sys.exit()
    if 'mean' in self.inputs['initial_coordinates_type'][1]:
      flag = flag+'mean'
    elif 'osc' in self.inputs['initial_coordinates_type'][1]:
      flag = flag+'osc'
    else:
      print('Error with the type of initial conditions')
      print(self.inputs['initial_coordinates_type'][1])
      sys.exit()
      
    self.call_parser('initial_condition_type' ,'  initial_condition_type="'+str(flag)+'"\n')

    # Integration
    self.call_parser('integrator_type'        ,'  integrator_type="'+str(self.integration['type'])+'"\n')
    t_year = 0
    t_day  = self.integration['time_day']
    t_sec  = self.integration['time_sec']
    self.call_parser('integration_time' ,'  integration_time= {0} {1} {2}\n'.format(t_year,t_day,t_sec))
    step = int(self.integration['step_day'])*(24*3600.)+float(self.integration['step_sec'])
    self.call_parser('integration_step'       ,'  integration_step='+str(step)+'\n')

    # Output
    self.call_parser('write_ephemeris'        ,'  write_ephemeris='+str(self.outputs['ephem_write'])+'\n')
    dt_year = 0
    dt_day  = self.outputs['output_step_day']
    dt_sec  = self.outputs['output_step_sec']
    self.call_parser('output_step_time'    ,'  output_step_time= {0} {1} {2}\n'.format(dt_year,dt_day,dt_sec))
    if int(self.outputs['short_periods'])==1:
      flag = 0
    else:
      flag = 1
    self.call_parser('mean_element_flag'      ,'  mean_element_flag='+str(flag)+'\n')
    self.call_parser('chaos_indicator'        ,'  chaos_indicator='+str(self.outputs['chaos_indicator'])+'\n')
    self.call_parser('write_atmosphere'       ,'  write_atmosphere='+str(self.outputs['write_atmosphere'])+'\n')

    if int(self.forces['station_keeping'])==1:
      self.call_parser('pPara','  pPara= 0          0     0    0      1 \n ')
      self.call_parser('station_keeping','  station_keeping=1 \n ')
    else:
      self.call_parser('pPara','  pPara= 0          0     0    0      0 \n ')
      self.call_parser('station_keeping','  station_keeping=0 \n ')      
    if self.outputs['coordinates_type'][0]=='kepler':
      self.call_parser('pCart','  pCart= 0   0   0   0    0    0    0 \n ')
      self.call_parser('pKepl','  pKepl=  0        1   1   1   1      1        1      0      0      0 \n')
    elif self.outputs['coordinates_type'][0]=='cartesian':
      self.call_parser('pCart','  pCart= 1   1   1   1    1    1    0 \n ')
      self.call_parser('pKepl','  pKepl=  0        0   0   0   0      0        0      0      0      0 \n')
    else:
      print('Error with output initial_coordinates_type')

    # Force (geopotential)
    self.call_parser('pDEGREMAX','  pDEGREMAX='+str(self.forces['degree_max'])+'\n')
    self.call_parser('pORDREMAX','  pORDREMAX='+str(self.forces['order_max'])+'\n')
    geo_degree2 = str(self.forces['harmonics'][0][0:5]).replace('[','').replace(']','').replace('.','')
    geo_degree3 = str(self.forces['harmonics'][1][0:7]).replace('[','').replace(']','').replace('.','')
    geo_degree4 = str(self.forces['harmonics'][2][0:9]).replace('[','').replace(']','').replace('.','')
    geo_degree5 = str(self.forces['harmonics'][3][0:11]).replace('[','').replace(']','').replace('.','')   
    self.call_parser('pd2','  pd2= '+geo_degree2+'                   !C20 C21 S21 C22 S22\n')
    self.call_parser('pd3','  pd3= '+geo_degree3+'             !C30 C31 S31 C32 S32 C33 S33 \n')
    self.call_parser('pd4','  pd4= '+geo_degree4+'       !C40 C41 S41 C42 S42 C43 S43 C44 S44 \n')
    self.call_parser('pd5','  pd5= '+geo_degree5+' !C50 C51 S51 C52 S52 C53 S53 C54 S54 C55 S55 \n')

    # Forces (third body)
    
    self.call_parser('third_body_flag','  third_body_flag=    '+str(self.forces['sun'])+'        '+str(self.forces['moon'])+'\n')
    self.call_parser('third_body_ephem','  third_body_ephem=\''+str(self.forces['ephemeris_3c'])+'\'\n')
    
    # Forces (drag)
    if self.forces['drag']=='J70':
      flag_drag = 1
    elif self.forces['drag']=='JB2008':
      flag_drag = 2
    elif self.forces['drag']=='DTM2013':
      flag_drag = 3
    elif self.forces['drag']=='TD88':
      flag_drag = 4
    else:
      flag_drag = 0
      
    self.call_parser('drag_model' ,'  drag_model='+str(flag_drag)+'\n')
    if flag_drag!=0:
      if self.forces['space_weather']=='constant':
        self.call_parser('space_weather_data','  space_weather_data='+str(1)+'\n')
      elif self.forces['space_weather']=='historical':
        self.call_parser('space_weather_data','  space_weather_data='+str(2)+'\n')        
      else:
        self.call_parser('space_weather_data','  space_weather_data='+str(0)+'\n')
    self.call_parser('coeff_drag'   ,'  coeff_drag='+str(self.forces['drag_coefficient'])+'\n')
    self.call_parser('f107_cst','f107_cst = '+str(self.forces['f107'])+'\n')
    self.call_parser('ap_cst','ap_cst = '+str(self.forces['ap'])+'\n') 
    
    # Forces (srp)    
    if self.forces['umbra_model']=='Cylinder':
      flag_umbra = 1
    elif self.forces['umbra_model']=='Penumbra':
      flag_umbra = 2
    else:
      flag_umbra = 0

    self.call_parser('srp_flag'   ,'  srp_flag='+str(self.forces['srp'])+'\n')
    self.call_parser('albedo'     ,'  albedo='+str(self.forces['albedo'])+'\n')
    self.call_parser('umbra_model','  umbra_model='+str(flag_umbra)+'\n')
    
    # Forces (manoeuvres)
    self.call_parser('manoeuvre_list','  manoeuvre_list='+str(self.forces['maoeuvres'])+'\n')
    self.call_parser('manoeuvre_file','  manoeuvre_file=\'manoeuvres.txt\'\n')    

    
  def call_parser(self,key,newline):
    """ Module to modify the parser.

    INPUTS
    ------

    key: string
      Key word of the parameter
    newline: string
      New value of the parameter

    """
    
    parserfile=open(self.name_output+'/input/parser.par', 'r')
    parser=parserfile.readlines()
    parserfile.close()
    for line in parser:
      if key in line :
        parserfile=open(self.name_output+'/input/parser.par', 'r')
        parserModif=parserfile.read()
        parserfile.close()
        m=parserModif.replace(line,newline)
    newParserfile=open(self.name_output+'/input/parser.par', 'w')  
    newParserfile.write(m)
    newParserfile.close()
    

  def propagation_with_nimastep(self):
    """ Compute the orbit with NIMASTEP.

    RETURN
    ------

    ephemeris: dataframe
      Contain the result of the propagation.

    """
    
    # Propagation
    command='cd ./'+self.name_output+'/ ; ./nimastep_8.0 > log_nimastep.txt'
    os.system(command)

    # Read ephemeris    
    id_norad = "%05d" % self.id_number
    path = self.name_output+'/output/'
    f = open(path+'ic_file_ephem_'+str(id_norad)+'.txt','r')
    data = f.readlines()
    f.close()
    headers = data[0].split()
    ephemeris = []
    for row in data[1:]:  
      ephemeris.append([float(item) for item in row.split()])
    ephemeris = pd.DataFrame(ephemeris,columns=headers)
    ephemeris['Date'] = ephemeris['JD'].apply(lambda x: jd_2_date(float(x)))
    
    return ephemeris


  ########################################################################
  #
  # WRAPPER COPAIN 4.2
  # 
  ########################################################################

  def copain(self,state_vector,output='computation'):
    """ Compute an orbit with COPAIN 4.1 (with FAST/SATlight/COPINE).

    INPUTS
    ------
    
    state_vector: dataframe
      Initial conditions.

    OPTIONAL INPUTS
    ---------------
    
    output: string
      Name of the directory containing the executable file.

    RETURN
    ------
 
    ephemeris: dataframe
      Evolution of the initial state vector.

    """

    #self.prop_dir = DB_env().get_cfg().get('Softwares','fast')
    self.name_output = output

    if os.path.isfile(self.prop_dir+'/bin/fast_4.0.x')==False:
      print('Error: Fast 4.1 is not present')
      sys.exit()
    
    if (os.path.exists(self.name_output)):
      os.system('rm -r '+self.name_output)
    os.system('mkdir '+self.name_output)
    os.system('cp -r '+self.prop_dir+'/exe ./'+self.name_output+'/')
    os.system('cp '+self.prop_dir+'/scripts/gofast.sh ./'+self.name_output+'/')
    self.modify_directory_file(state_vector)

    ephemeris = self.propagation_with_copain()
    
    if int(self.outputs['clean'])==1:
      os.system('rm -r ./'+self.name_output)
    
    return ephemeris


  def modify_directory_file(self,state_vector):
    """ Fill the directory file.

    INPUTS
    ------
    
    state_vector: dataframe
      Initial conditions.

    """

    #PERTURBATION POTENTIEL CENTRAL

    if int(self.forces['degree_max'])>0 or int(self.forces['order_max'])>0:
      key='i_pot'
      geo_flag='i_pot=1'
      self.modify_line_ficdir(key,geo_flag)
      if int(self.forces['degree_max'])>1:
        key='i_zon'
        geo_flag='i_zon=1'
        self.modify_line_ficdir(key,geo_flag)
        key='degmin_zon'
        geo_flag='degmin_zon=2'
        self.modify_line_ficdir(key,geo_flag)        
        key='degmax_zon'
        geo_flag='degmax_zon='+str(self.forces['degree_max'])
        self.modify_line_ficdir(key,geo_flag)       
      else:
        key='i_zon'
        geo_flag='i_zon=0'
        self.modify_line_ficdir(key,geo_flag)
      if int(self.forces['order_max'])>0:
        key='i_tes'
        geo_flag='i_tes=1'
        self.modify_line_ficdir(key,geo_flag)
        key='degmin_tes'
        geo_flag='degmin_tes=0'
        self.modify_line_ficdir(key,geo_flag)        
        key='degmax_tes'
        geo_flag='degmax_tes='+str(self.forces['order_max'])
        self.modify_line_ficdir(key,geo_flag)       
      else:
        key='i_tes'
        geo_flag='i_tes=0'
        self.modify_line_ficdir(key,geo_flag)
    else:
      key='i_pot'
      geo_flag='i_pot=0'
      self.modify_line_ficdir(key,geo_flag)

    #PERTURBATION POTENTIEL EXTERNE
   
    if (int(self.forces['sun'])==1 or int(self.forces['moon'])==1):
      key='i_ext'
      tb_flag='i_ext=1'
      self.modify_line_ficdir(key,tb_flag)
      if int(self.forces['sun'])==1:
        key='i_soleil'
        tb_flag='i_soleil=1'
        self.modify_line_ficdir(key,tb_flag)
      else:
        key='i_soleil'
        tb_flag='i_soleil=0'
        self.modify_line_ficdir(key,tb_flag)
      if int(self.forces['moon'])==1:
        key='i_lune'
        tb_flag='i_lune=1'
        self.modify_line_ficdir(key,tb_flag)
      else:
        key='i_lune'
        tb_flag='i_lune=0'
        self.modify_line_ficdir(key,tb_flag)
    else:
      key='i_ext'
      tb_flag='i_ext=0'
      self.modify_line_ficdir(key,tb_flag)        
        
    # PERTURBATION PRESSION DE RADIATION SOLAIRE

    if (int(self.forces['srp'])==1):
      key='i_prs'
      srp_flag='i_prs=1'
      self.modify_line_ficdir(key,srp_flag)
      key='prs_mode'
      srp_flag='prs_mode=\''+self.forces['srp_method']+'\''
      self.modify_line_ficdir(key,srp_flag)      
      key='npts_quad_prs'
      srp_flag='npts_quad_prs='+str(self.forces['pts_quadrature_srp'])
      self.modify_line_ficdir(key,srp_flag)
      key='fourier_rang_max_prs'
      srp_flag='fourier_rang_max_prs='+str(self.forces['fourier_rang_max_prs'])
      self.modify_line_ficdir(key,srp_flag)
      if self.forces['umbra_model']=='Cylinder':
        key='ombre_modele'
        srp_flag='ombre_modele=\''+self.forces['umbra_model']+'\''
        self.modify_line_ficdir(key,srp_flag)        
      else:
        key='i_ombre'
        srp_flag='i_ombre=1'
        self.modify_line_ficdir(key,srp_flag)
    else:
      key='i_prs'
      srp_flag='i_prs=0'
      self.modify_line_ficdir(key,srp_flag)

    # PERTURBATION FREINAGE ATMOSPHERIQUE
      
    key = 'Prise en compte de la perturbation du freinage'
    if self.forces['drag'] in ['J70','JB2008','DTM2013','TD88']:
      key ='i_drag'
      fr_flag='i_drag=1'
      self.modify_line_ficdir(key,fr_flag)
      key='nom_atm_mod'
      fr_flag='nom_atm_mod=\''+self.forces['drag']+'\''
      self.modify_line_ficdir(key,fr_flag)
      key='npts_quad_fr'
      fr_flag='npts_quad_fr='+str(self.forces['pts_quadrature_fr'])
      self.modify_line_ficdir(key,fr_flag)      
      key='fourier_rang_max_fr'
      fr_flag='fourier_rang_max_fr='+str(self.forces['fourier_rang_max_fr'])
      self.modify_line_ficdir(key,fr_flag)
    else:
      key ='i_drag'
      fr_flag='i_drag=0'
      self.modify_line_ficdir(key,fr_flag)      
      
    # SPACE WEATHER

    key='type_acsol'
    #if self.forces['space_weather']=='historical':
    fr_flag='type_acsol=\'hist+cst\''
    self.modify_line_ficdir(key,fr_flag)
    #else:
    #  fr_flag='type_acsol=\''+self.forces['space_weather']+'\''
    #  self.modify_line_ficdir(key,fr_flag)
    if self.forces['space_weather']=='constant':
      key='fbar_lu'
      fr_flag='fbar_lu=150'
      self.modify_line_ficdir(key,fr_flag)      
      key='ap_lu'
      fr_flag='ap_lu=5'
      self.modify_line_ficdir(key,fr_flag)
    elif self.forces['space_weather']=='historical':
      key='nom_fic_acsol'
      fr_flag='nom_fic_acsol=\'space_weather.dat\''
      self.modify_line_ficdir(key,fr_flag)

    """  
    key='alt_rentree'
    fr_flag='alt_rentree=120'
    self.modify_line_ficdir(key,fr_flag)
    """
              
    # DATES DEBUT/FIN

    ti = state_vector.iloc[0]['Date']
    jd = date_2_jd(ti)
    jd_cnes,sec_cnes = jd_2_jd_cnes(jd)
    key='jj_deb'
    t_flag='jj_deb='+str(jd_cnes)
    self.modify_line_ficdir(key,t_flag)
    key='sec_deb'
    t_flag='sec_deb='+str(sec_cnes)
    self.modify_line_ficdir(key,t_flag)

    dt = int(self.integration['time_day'])+float(self.integration['time_sec'])/(24*3600.) 
    tf = ti + timedelta(days=dt)
    jd = date_2_jd(tf)
    jd_cnes,sec_cnes = jd_2_jd_cnes(jd)
    key='jj_fin'
    t_flag='jj_fin='+str(jd_cnes)
    self.modify_line_ficdir(key,t_flag)
    key='sec_fin'
    t_flag='sec_fin='+str(sec_cnes)
    self.modify_line_ficdir(key,t_flag)
    
    # BULLETIN
    
    if self.inputs['initial_coordinates_type'][0]=='tle':
      key='modbul'
      in_flag='modbul=1'
      self.modify_line_ficdir(key,in_flag)
      key     = 'line1='
      in_flag = 'line1='+state_vector.iloc[0]['L1']
      self.modify_line_ficdir(key,in_flag)
      key     = 'line2='
      in_flag = 'line2='+state_vector.iloc[0]['L2']
      self.modify_line_ficdir(key,in_flag)      
    else:
      
      key='modbul'
      in_flag='modbul=0'
      self.modify_line_ficdir(key,in_flag)
      
      if 'kepler'==self.inputs['initial_coordinates_type'][0]:
        key='natbul'
        in_flag='natbul=\'kepler\''
        self.modify_line_ficdir(key,in_flag)
      elif 'cartesian'==self.inputs['initial_coordinates_type'][0]:
        key='natbul'
        in_flag='natbul=\'posvit\''
        self.modify_line_ficdir(key,in_flag)
      else:
        print('Error: bad inputs')
        sys.exit()
        
      if 'mean'==self.inputs['initial_coordinates_type'][1]:
        key='typvar'
        in_flag='typvar=\'moy\''
        self.modify_line_ficdir(key,in_flag)
      elif 'osc'==self.inputs['initial_coordinates_type'][1]:
        key='typvar'
        in_flag='typvar=\'osc\''
        self.modify_line_ficdir(key,in_flag)

      sma   = state_vector.iloc[0]['a[m]']
      ecc   = state_vector.iloc[0]['e']    
      inc   = state_vector.iloc[0]['i[deg]']
      raan  = state_vector.iloc[0]['RAAN[deg]']
      omega = state_vector.iloc[0]['Omega[deg]']
      ma    = state_vector.iloc[0]['MA[deg]']
    
      key = 'veceta'
      in_flag = 'veceta = {0} {1} {2} {3} {4} {5}'.format(sma,ecc,inc,raan,omega,ma)
      self.modify_line_ficdir(key,in_flag)    
      bc      = state_vector.iloc[0]['BC[m2/kg]']      
      key     = 'surface'
      in_flag = 'surface='+str(bc/2.2)
      self.modify_line_ficdir(key,in_flag)
      key     = 'masse'
      in_flag = 'masse='+str(1.00)
      self.modify_line_ficdir(key,in_flag)    

    # INTEGRATION DES EQUATIONS DU MOUVEMENT

    key = 'typint'
    int_flag = 'typint=\''+self.integration['fast_type']+'\''
    self.modify_line_ficdir(key,int_flag)  
    jour = self.integration['step_day']
    sec  = self.integration['step_sec']
    key  = 'jpas'
    int_flag = 'jpas='+self.integration['step_day']
    self.modify_line_ficdir(key,int_flag)    
    key  = 'hpas'
    int_flag = 'hpas='+self.integration['step_sec']
    self.modify_line_ficdir(key,int_flag)       
    key = 'type_integrateur'
    int_flag = 'type_integrateur=\''+self.integration['type']+'\''
    self.modify_line_ficdir(key,int_flag)
    
    # GESTION DES COURTES PERIODES

    key='ajout_cp'
    out_flag='ajout_cp='+str(self.outputs['short_periods'])
    self.modify_line_ficdir(key,out_flag)
    key='supr_cp'
    out_flag='supr_cp='+str(self.outputs['short_periods_suppr'])
    self.modify_line_ficdir(key,out_flag)
  
    # GESTION DES SORTIES

    key='nbj'
    out_flag='nbj='+self.outputs['output_step_day']
    self.modify_line_ficdir(key,out_flag)    
    key='pas_ech'
    out_flag='pas_ech='+self.outputs['output_step_sec']
    self.modify_line_ficdir(key,out_flag)
    key='type_elts_sortie'
    if 'cartesian'==self.outputs['coordinates_type'][0]:
      out_flag='type_elts_sortie=\'posvit\''
    elif 'kepler'==self.outputs['coordinates_type'][0]:
      out_flag='type_elts_sortie=\'kepler\''
    else:
      print('Error: bad output')
      sys.exit()
    self.modify_line_ficdir(key,out_flag)

    
  def modify_line_ficdir(self,key,newline):
    """ Modify a line of the file directory for FAST.

    INPUTS
    ------

    key: string
      Key word of the parameter
    newline: string
      New value of the parameter

    """
  
    file_directory = open('./'+self.name_output+'/exe/DIRF_test','r')
    ficdir = file_directory.readlines()
    file_directory.close()
    m = None
    for line in ficdir:
      if key in line :
        file_directory = open('./'+self.name_output+'/exe/DIRF_test','r')
        ficdir_mod = file_directory.read()
        file_directory.close()
        #print(line,str(newline))
        m = ficdir_mod.replace(line,str(newline)+'\n')

    if m==None:        
      print('The key is not in the file :')
      print(key)
      sys.exit()
      
    file_directory = open('./'+self.name_output+'/exe/DIRF_test','w')  
    file_directory.write(m)
    file_directory.close()

    
  def propagation_with_copain(self):
    """ Compute the orbit with COPAIN.

    RETURN
    ------

    ephemeris: dataframe
      Contain the result of the propagation.

    """
    
    # Propagation
    path = os.getcwd()
    if False:
      command='cd ./'+self.name_output+'; bash gofast.sh -f exe/DIRF_test'
    else:
      command='cd ./'+self.name_output+'; bash gofast.sh -f exe/DIRF_test > log_fast.txt'      
    os.system(command)
    
    # Read ephemeris    
    path = './'+self.name_output+'/exe/'
    f = open(path+'ORBF_test','r')
    d = f.readlines()
    f.close()
    
    if 'numerique' in d[3]:
      i0 = 32
    elif 'semi-analytiq' in d[13]:
      if self.outputs['coordinates_type'][0]=='kepler':
        i0 = 42
      else:
        i0 = 39
    elif 'analytique' in d[5]:
      i0 = 40
    else:
      print('Mauvais type de fichier')
      for i in range(0,50):
        print(i,d[i])
      sys.exit()

    if self.inputs['initial_coordinates_type'][0]=='tle':
      i0 = 31

    if self.outputs['coordinates_type'][0]=='kepler':
      header = ["JD","a[m]","e","i[deg]","RAAN[deg]","Omega[deg]","MA[deg]"]
    elif self.outputs['coordinates_type'][0]=='cartesian':
      header = ["JD","x[m]","y[m]","z[m]","vx[m]","vy[m]","vz[m]"]
    else:
      print('Error: bad outputs')
      sys.exit()
    ephemeris = []
    #print('i0 = ',i0)
    for i,row in enumerate(d[i0:]):
      #print(i0+i,row)
      row = row.split()
      jj_cnes = float(row[3])
      jj = 2433282.5 + jj_cnes
      sma = float(row[4])
      if np.isnan(sma):
        print('Error: Nan')
        sys.exit()
      ecc = float(row[5])
      inc = float(row[6])
      raan = float(row[7])
      omega = float(row[8])
      ma = float(row[9])%360
      ephemeris.append([jj,sma,ecc,inc,raan,omega,ma])
    ephemeris = pd.DataFrame(ephemeris,columns=header)
    #ephemeris = ephemeris.convert_objects(convert_numeric=True)
    ephemeris['Date'] = ephemeris['JD'].apply(lambda x: jd_2_date(float(x)))
    
    return ephemeris
      
  
  ########################################################################
  #
  # WRAPPER STELA 3.2 
  #
  ######################################################################## 
  
  def stela(self,state_vector,output='stela'):
    """ Compute the orbit with STELA.

    INPUTS
    ------
    
    state_vector: dataframe
      Initial conditions.

    OPTIONAL INPUTS
    ---------------

    output: string
      Working directory
    
    RETURN
    ------
 
    ephemeris: dataframe
      Evolution of the initial state vector.

    """
   
    #local_db = DB_env()    
    #self.propagator_directory = local_db.get_cfg().get('Softwares','stela')
    self.name_output = output
    
    if (os.path.exists(self.name_output)):
      os.system('rm -r '+self.name_output)
    os.system('mkdir '+self.name_output)
    os.system('cp -r '+self.propagator_directory+'/* ./'+self.name_output+'/')
    
    self.modify_xml(state_vector)

    ephemeris = self.propation_with_stela()

    if int(self.outputs['clean'])==1:
      os.system('rm -r ./'+self.name_output)

    return ephemeris

  
  def modify_xml(self,state_vector):
    """ Modify the file of initial conditions for Stela.

    INPUTS
    ------

    state_vector: dataframe
      Initial conditions.

    """
    
    # Initial conditions
    date_str = str(state_vector.iloc[0]["Date"])
    date     = date_str[0:10]+'T'+date_str[11:19]+'.00'
    sma      = state_vector.iloc[0]["a[m]"]                    
    ecc      = state_vector.iloc[0]["e"]
    inc      = state_vector.iloc[0]["i[deg]"]*np.pi/180.     #rad
    raan     = state_vector.iloc[0]["RAAN[deg]"]*np.pi/180.  #rad
    omega    = state_vector.iloc[0]["Omega[deg]"]*np.pi/180. #rad
    ma       = state_vector.iloc[0]["MA[deg]"]*np.pi/180.    #rad
    bc       = state_vector.iloc[0]["BC[m2/kg]"]
    am       = bc/float(self.inputs['Cd'])
   
    # Read xml file
    if (sma*(1-ecc)-earth_eq_radius)/1000.<30000.:
      doc = minidom.parse('./'+self.name_output+'/examples/example_LEO_sim.xml')
      region = 'leo'
    elif (ecc>0.2):
      doc = minidom.parse('./'+self.name_output+'/examples/example_GTO_sim.xml')
      region = 'gto'
    else:
      doc = minidom.parse('./'+self.name_output+'/examples/example_GEO_sim.xml')
      region = 'geo' 
      
    # Equinoctial elements
    ex = str(ecc*np.cos(raan+omega))
    ey = str(ecc*np.sin(raan+omega))
    ix = str(np.sin(inc/2.)*np.cos(raan))
    iy = str(np.sin(inc/2.)*np.sin(raan))
    la = str(raan+omega+ma)
     
    root = doc.documentElement
 
    for element in root.getElementsByTagName('EphemerisManager'):
      new_element = element.childNodes[1].childNodes[1]
      for element in new_element.getElementsByTagName('date'):
        element.firstChild.replaceWholeText(date)
        
    if (sma*(1-ecc)-earth_eq_radius)/1000.<15000.:

      for element in root.getElementsByTagName('EphemerisManager'):
        new_element = element.childNodes[1].childNodes[1]
        for element in new_element.getElementsByTagName('Type2PosVel'):
          for element in element.getElementsByTagName('semiMajorAxis'):
            element.firstChild.replaceWholeText(sma)
        
      for element in root.getElementsByTagName('EphemerisManager'):
        new_element = element.childNodes[1].childNodes[1]
        for element in new_element.getElementsByTagName('Type2PosVel'):
          for element in element.getElementsByTagName('eccentricity'):
            if ecc==0:
              element.firstChild.replaceWholeText('0.0')
            else:
              element.firstChild.replaceWholeText(ecc)
            
      for element in root.getElementsByTagName('EphemerisManager'):
        new_element = element.childNodes[1].childNodes[1]
        for element in new_element.getElementsByTagName('Type2PosVel'):
          for element in element.getElementsByTagName('inclination'):
            if inc==0:
              element.firstChild.replaceWholeText('0.0')
            else:
              element.firstChild.replaceWholeText(inc)

      for element in root.getElementsByTagName('EphemerisManager'):
        new_element = element.childNodes[1].childNodes[1]
        for element in new_element.getElementsByTagName('Type2PosVel'):
          for element in element.getElementsByTagName('rAAN'):
            if raan==0:
              element.firstChild.replaceWholeText('0.0')
            else:
              element.firstChild.replaceWholeText(raan)
          
      for element in root.getElementsByTagName('EphemerisManager'):
        new_element = element.childNodes[1].childNodes[1]
        for element in new_element.getElementsByTagName('Type2PosVel'):
          for element in element.getElementsByTagName('argOfPerigee'):
            if omega==0:
              element.firstChild.replaceWholeText('0.0')
            else:
              element.firstChild.replaceWholeText(omega)

      for element in root.getElementsByTagName('EphemerisManager'):
        new_element = element.childNodes[1].childNodes[1]
        for element in new_element.getElementsByTagName('Type2PosVel'):
          for element in element.getElementsByTagName('meanAnomaly'):
            if ma==0:
              element.firstChild.replaceWholeText('0.0')
            else:        
              element.firstChild.replaceWholeText(ma)
     
    else:

      for element in root.getElementsByTagName('EphemerisManager'):
        new_element = element.childNodes[1].childNodes[1]
        for element in new_element.getElementsByTagName('Type8PosVel'):
          for element in element.getElementsByTagName('semiMajorAxis'):
            element.firstChild.replaceWholeText(sma)

      for element in root.getElementsByTagName('EphemerisManager'):
        new_element = element.childNodes[1].childNodes[1]
        for element in new_element.getElementsByTagName('Type8PosVel'):
          for element in element.getElementsByTagName('lambdaEq'):
            if raan==0:
              element.firstChild.replaceWholeText('0.0')
            else:
              element.firstChild.replaceWholeText(la)

      for element in root.getElementsByTagName('EphemerisManager'):
        new_element = element.childNodes[1].childNodes[1]
        for element in new_element.getElementsByTagName('Type8PosVel'):
          for element in element.getElementsByTagName('eX'):
            if raan==0:
              element.firstChild.replaceWholeText('0.0')
            else:
              element.firstChild.replaceWholeText(ex)

      for element in root.getElementsByTagName('EphemerisManager'):
        new_element = element.childNodes[1].childNodes[1]
        for element in new_element.getElementsByTagName('Type8PosVel'):
          for element in element.getElementsByTagName('eY'):
            if raan==0:
              element.firstChild.replaceWholeText('0.0')
            else:
              element.firstChild.replaceWholeText(ey)

      for element in root.getElementsByTagName('EphemerisManager'):
        new_element = element.childNodes[1].childNodes[1]
        for element in new_element.getElementsByTagName('Type8PosVel'):
          for element in element.getElementsByTagName('iX'):
            if raan==0:
              element.firstChild.replaceWholeText('0.0')
            else:
              element.firstChild.replaceWholeText(ix)

      for element in root.getElementsByTagName('EphemerisManager'):
        new_element = element.childNodes[1].childNodes[1]
        for element in new_element.getElementsByTagName('Type8PosVel'):
          for element in element.getElementsByTagName('iY'):
            if raan==0:
              element.firstChild.replaceWholeText('0.0')
            else:
              element.firstChild.replaceWholeText(iy)

    for element in root.getElementsByTagName('SpaceObject'):
      element.childNodes[1].firstChild.replaceWholeText(1.)
      element.childNodes[3].firstChild.replaceWholeText(am)
      element.childNodes[5].firstChild.replaceWholeText(am)
      element.childNodes[7].firstChild.replaceWholeText(1.)

    # Integration parameters
    for element in root.getElementsByTagName('simulationDuration'):
      time_day = float(self.integration['time_day'])
      time_sec = float(self.integration['time_sec'])      
      time = (time_day+time_sec/(24.*3600.))/365.25
      element.firstChild.replaceWholeText(time) #year

    for element in root.getElementsByTagName('ephemerisStep'):
      step_day = float(self.outputs['output_step_day'])
      step_sec = float(self.outputs['output_step_sec'])
      step = step_day*86400.+step_sec
      element.firstChild.replaceWholeText(step) #secondes

    for element in root.getElementsByTagName('integrationStep'):
      step_day = float(self.integration['step_day'])
      step_sec = float(self.integration['step_sec'])
      step = step_day*86400.+step_sec
      element.firstChild.replaceWholeText(step) #secondes
      
    # Forces

    for element in root.getElementsByTagName('srpSwitch'):
      if int(self.forces['srp'])>0:
        element.firstChild.replaceWholeText('true')
      else:
        element.firstChild.replaceWholeText('false')
    
    for element in root.getElementsByTagName('sunSwitch'):
      if int(self.forces['sun'])>0:
        element.firstChild.replaceWholeText('true')
      else:
        element.firstChild.replaceWholeText('false')
        
    for element in root.getElementsByTagName('moonSwitch'):
      if int(self.forces['moon'])>0:
        element.firstChild.replaceWholeText('true')
      else:
        element.firstChild.replaceWholeText('false')
    
    for element in root.getElementsByTagName('dragSwitch'):
      if ((int(self.forces['drag'])>0) and (region!='gto')):
        element.firstChild.replaceWholeText('true')
      else:
        element.firstChild.replaceWholeText('false')

    for element in root.getElementsByTagName('zonalSwitch'):
      if int(self.forces['degree_max'])>1:
        element.firstChild.replaceWholeText('true')
        for element in root.getElementsByTagName('zonalOrder'):
          element.firstChild.replaceWholeText(self.forces['degree_max'])
      else:
        element.firstChild.replaceWholeText('false')
        for element in root.getElementsByTagName('zonalOrder'):
          element.firstChild.replaceWholeText('0')

    for element in root.getElementsByTagName('tesseralSwitch'):
      if int(self.forces['order_max'])>0:
        element.firstChild.replaceWholeText('true')
        for element in root.getElementsByTagName('tesseralOrder'):
         element.firstChild.replaceWholeText(self.forces['order_max'])
      else:
        element.firstChild.replaceWholeText('false')            
        for element in root.getElementsByTagName('tesseralOrder'):
         element.firstChild.replaceWholeText('0')
        
    # Save the modification in a new xml file
    file_handle = open(self.name_output+'/new_input.xml',"w")
    root.writexml(file_handle)
    file_handle.close()
    
    
  def propation_with_stela(self):
    """ Compute the orbit with Stela. 

    RETURN
    ------
  
    ephemeris : dataframe
      The evolution of the initial state vector.
  
    """
    
    input_param        = 'new_input.xml'
    input_file         = 'eph_stela'
    output_file_prefix = 'output'

    # Propagation
    command='cd ./'+self.name_output+' ; sh bin/stela-batch.sh -i '+input_param+' -'+input_file+' '+output_file_prefix+' mean keplerian TEME > /dev/null'
    os.system(command)    
    
    # Read ephemeris
    ephemeris = []   
    f = open('./'+self.name_output+'/'+output_file_prefix+'_eph.txt','r')
    data = f.readlines()
    f.close()

    for k in range(20,len(data)-1,1):
      jd = float(data[k].split()[0])+float(data[k].split()[1])/(86400.)+2400000.5
      date = jd_2_date(jd)
      sma = float(data[k].split()[2])*1000.
      ecc = float(data[k].split()[3])
      inc = float(data[k].split()[4])
      raan = float(data[k].split()[5])
      omega = float(data[k].split()[6])
      ma = float(data[k].split()[7])
      ephemeris.append([date,sma,ecc,inc,raan,omega,ma])
    headers=["Date","a[m]","e","i[deg]","RAAN[deg]","Omega[deg]","MA[deg]"]
    ephemeris = pd.DataFrame(ephemeris,columns=headers)

    return ephemeris


########################################################################
#
# WRAPPER Thalassa
#
########################################################################


  def thalassa(self,state_vector,coord='kepl',output='thalassa'):
    """ Compute an orbit with Thalassa.

    Ref: https://gitlab.com/souvlaki/thalassa

    INPUTS
    ------
    
    state_vector: dataframe
      Initial conditions (keplerian or cartesian elements).

    OPTIONAL INPUTS
    ---------------

    coord: string
      Type of coordinates used as inputs.
    output: string
      Name of the directory storing the results.
    
    RETURN
    ------
 
    ephemeris: dataframe
      Evolution of the initial state vector.

    """

    #local_db = DB_env()
    #self.propagator_directory = local_db.get_cfg().get('Softwares','thalassa')
    self.name_output = output
    
    if (os.path.exists(self.name_output)):
      os.system('rm -r '+self.name_output)
    os.system('mkdir '+self.name_output)
    os.system('mkdir '+self.name_output+'/results')
    os.system('cp -r '+self.propagator_directory+'/* ./'+self.name_output+'/')

    self.fill_input_thalassa()
    self.fill_object_thalassa(state_vector)
    ephemeris = self.propagation_with_thalassa()

    if int(self.outputs['clean'])==1:
      os.system('rm -r ./'+self.name_output)
    
    return ephemeris  

  
  def fill_input_thalassa(self):
    """ Modify the input file of Thalassa.
    """
    
    # Forces (third body)
    name_file = 'in/input.txt'
    if self.forces['degree_max']>0 or self.forces['order_max']>0 :
      value = '1'
    else:
      value = '0'
    self.modify_line('insgrav:  ','insgrav:   '+value,name_file)
    value = str(self.forces['sun'])
    self.modify_line('isun:     ','isun:      '+value,name_file)
    value = str(self.forces['moon'])
    self.modify_line('imoon:    ','imoon:     '+value,name_file)
    if self.forces['drag']>0:
      value = '1'
    else:
      value = '0'
    self.modify_line('idrag:    ','idrag:     '+value,name_file)
    if self.forces['srp']>0:
      value = '1'
    else:
      value = '0'
    self.modify_line('iSRP:     ','iSRP:      '+value,name_file)
    value = '2'
    self.modify_line('iephem:   ','iephem:    '+value,name_file)
    value = str(self.forces['degree_max'])
    self.modify_line('gdeg:     ','gdeg:      '+value,name_file)
    value = str(self.forces['order_max'])
    self.modify_line('gord:     ','gord:      '+value,name_file)
    

    value = '%.15E' % float(1.E-12)
    self.modify_line('tol:      ','tol:       '+value,name_file)
    time = float(self.integration['time_day'])
    time += float(self.integration['time_sec'])/86400.
    value = '%.15E' % float(time)
    self.modify_line('tspan:    ','tspan:     '+value,name_file)
    step = float(self.outputs['output_step_day'])
    step += float(self.outputs['output_step_sec'])/86400. 
    value = '%.15E' % float(step)
    self.modify_line('tstep:    ','tstep:     '+value,name_file)

    #ratio = int(self.integration['time']*365.25/self.integration['step'])*100000   
    #value = '%.15E' % float(ratio)
    #print('Ratio = ',value)
    #self.modify_line('mxstep:   ','mxstep:    '+value,name_file)
    #value = str(1)
    #self.modify_line('eqs: ','eqs:   '+value,name_file)    

    path =  os.getcwd()
    value = path+'/'+self.name_output+'/results/'
    self.modify_line('out: ','out:   '+value,name_file)
    
        
  def fill_object_thalassa(self,state_vector):
    """ Modify the object file of Thalassa.

    INPUTS
    ------
    
    state_vector: dataframe
      Initial conditions.

    """

    mjd = date_2_jd(state_vector.iloc[0]['Date'])-2400000.0
    sma = state_vector.iloc[0]['a[m]']
    ecc = state_vector.iloc[0]['e']    
    inc = state_vector.iloc[0]['i[deg]']
    raan = state_vector.iloc[0]['RAAN[deg]']
    omega = state_vector.iloc[0]['Omega[deg]']
    ma = state_vector.iloc[0]['MA[deg]']
    bc = state_vector.iloc[0]['BC[m2/kg]']

    name_file = 'in/object.txt'

    value = '+%.15E' % float(mjd)
    self.modify_line('MJD  [UTC]',value+'; MJD  [UTC]',name_file)
    value = '+%.15E' % float(sma/1000.)
    self.modify_line('SMA  [km]' ,value+'; SMA  [km]',name_file)
    value = '+%.15E' % float(ecc)
    self.modify_line('ECC  [-]'  ,value+'; ECC  [-]',name_file)
    value = '+%.15E' % float(inc)
    self.modify_line('INC  [deg]',value+'; INC  [deg]',name_file)
    value = '+%.15E' % float(raan)
    self.modify_line('RAAN [deg]',value+'; RAAN [deg]',name_file)
    value = '+%.15E' % float(omega)
    self.modify_line('AOP  [deg]',value+'; AOP  [deg]',name_file)
    value = '+%.15E' % float(ma)
    self.modify_line('M    [deg]',value+'; M    [deg]',name_file)

    value = '+%.15E' % float(1.)
    self.modify_line('Mass [kg]'        ,value+'; Mass [kg] \n',name_file)
    value = '+%.15E' % float(bc/2.2)
    self.modify_line('Area (drag) [m^2]',value+'; Area (drag) [m^2] \n',name_file)
    value = '+%.15E' % float(bc/2.2)
    self.modify_line('Area (SRP)  [m^2]',value+'; Area (SRP)  [m^2] \n',name_file)
    value = '+%.15E' % float(2.2)
    self.modify_line('CD   [-]'         ,value+'; CD   [-] \n',name_file)     
    #self.modify_line('CR   [-]'         ,value+'; CR   [-] \n',name_file)

    
  def modify_line(self,key,newline,name_file):
    """ Modify a line of the file.

    INPUTS
    ------

    key: string
      Key word of the parameter
    newline: string
      New value of the parameter
    name_file: string
      Name of the file

    """
  
    file_directory = open('./'+self.name_output+'/'+name_file,'r')
    ficdir = file_directory.readlines()
    file_directory.close()
    m = None
    for line in ficdir:
      if key in line :
        file_directory = open('./'+self.name_output+'/'+name_file,'r')
        ficdir_mod = file_directory.read()
        file_directory.close()
        m = ficdir_mod.replace(line,str(newline)+'\n')

    if m==None:        
      print('The key is not in the file :')
      print(key)
      sys.exit()
      
    file_directory = open('./'+self.name_output+'/'+name_file,'w')  
    file_directory.write(m)
    file_directory.close()    

    
  def propagation_with_thalassa(self):
    """ Compute the orbit with Thalassa.

    RETURN
    ------

    ephemeris: dataframe
      Contain the result of the propagation.

    """

    #self.prop_dir = DB_env().get_cfg().get('Softwares','thalassa')

    if os.path.isfile(self.prop_dir+'/thalassa.x')==False:
      print('Error: Thalassa 4.1 is not present')
      sys.exit()
    
    # Propagation
    command='cd ./'+self.name_output+'/ ; ./thalassa.x > /dev/null'
    os.system(command)

    # Read ephemeris
    f = open('./'+self.name_output+'/results/orbels.dat','r')
    data = f.readlines()
    f.close()

    headers = ['JD',"a[m]","e","i[deg]","RAAN[deg]","Omega[deg]","MA[deg]"]
    ephemeris = []
    for row in data[3:]:
      row = row.strip().split(',')
      row = [float(item) for item in row[0:-1]]
      row[0] = row[0]+2400000
      ephemeris.append(row)
    ephemeris = pd.DataFrame(ephemeris,columns=headers)    
    ephemeris['Date'] = ephemeris['JD'].apply(lambda x: jd_2_date(float(x)))
    ephemeris['a[m]'] = ephemeris['a[m]']*1000.
    
    return ephemeris    
