#!/usr/bin/python
# Licensed under a 3-clause BSD style license - see LICENSE.rst
# -*-coding:Utf-8 -*

"""
  space_debris_model.py
  Author : Alexis Petit, Post-doc, IFAC-CNR, IMCCE, Observatoire de Paris
  Date : 2018 12 26
  This script is a part of CelestialPy. It allows to create a space debris
  population using orbit propagator and source model.

  Algorithm:
  
       read_initial_population ---|
                                  |
       read_source_scheduler -----|
                                  |
                                  |
                    |---> nasa_breakup_model
                    |             |
                    |     (new population)
                    |             |
                    |     orbit_propagator
                    |             |
                    |    (updated population)
                    |             |
                    |---<---------|
                                  |
                          (final_population)
              
"""


#Packages


import argparse
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import time
import sys
import os
import copy
from datetime import datetime,timedelta


# Packages CelestialPy


sys.path.insert(0,os.path.abspath(os.path.join(os.path.dirname(__file__),'..')))

from celestialpy_lite import earth_eq_radius,earth_mu
from celestialpy_lite import propagate
from celestialpy_lite import convert_ephemeris_2_snapshot
#from celestialpy_lite import wrapper_nbm

pd.set_option('mode.chained_assignment', None)

# Code


__all__ = [
  'space_debris_model'
]

  
def space_debris_model(work_dir,parameters):
  """ Create a space debris population.

  INPUTS
  ------

  work_dir: string
    Working directory.
  parameters: dictionary
    Parameters of the space debris model.

  RETURN
  ------
  
  population: dataframe
    Space debris population.

  """

  print('')
  print('Space debris model')
  print('------------------------')
  
  # Initialisation

  pop_dir = work_dir+'/'+parameters['snapshot_dir']
  if os.path.isdir(pop_dir):
    os.system('rm -r '+pop_dir)
  os.system('mkdir '+pop_dir)  
  
  # Read the initial population

  if parameters['initial_population']!=None:
    population = pd.read_csv(parameters['initial_population'])
    population['Date'] = pd.to_datetime(population['Date'],format='%Y-%m-%d %H:%M:%S')
    parameters['ti'] = population['Date'].iloc[0]
    
  # Read events

  if parameters['event_flag']==True:
    events = pd.read_csv(parameters['breakup_events'])
    events['Date'] = pd.to_datetime(events['Date'],format='%Y-%m-%d %H:%M:%S')
    events = events[events['Date']>parameters['ti']]
    counter_event = 0
    if parameters['initial_population']==None:
      parameters['ti'] = events['Date'].iloc[0]
      
    print('List of events')
    print(events)
    
  # Main loop

  print('')
  print(' > Initial date: ',parameters['ti'])
  
  t = parameters['ti']
  while(t<parameters['tf']):

    # Event / Create new space debris
    if parameters['event_flag']==True:
      if events.iloc[counter_event]['Date']==t:
        fragments = wrapper_nbm(work_dir,parameters['path_nbm'],events[counter_event:counter_event+1],parameters['minimal_size'])
        print(' > {0} fragments created'.format(len(fragments)))
        if (parameters['ti']!=t) and (len(fragments)>0) : 
          population = merge_population(parameters['headers'],population,fragments)
        else:
          population = copy.deepcopy(fragments)
          population['Id Norad'] = 1
          for i in range(0,len(population),1):
            population['Id Norad'].iloc[i] += i 
        if counter_event<len(events)-1:
          counter_event += 1
      t_event = events.iloc[counter_event]['Date']

      #print 'Initial date: ',t      
      if (t_event>t) and (t_event<parameters['tf']):
        time = t_event-t
        time_day = time.days
        time_sec = 0
        #print 'Next date:',events.iloc[counter_event]['Date']
      else:
        time = parameters['tf']-t
        time_day = time.days
        time_sec = 0
        #print 'Final date:',parameters['tf']
        
    else:
      time = parameters['tf']-t
      time_day = time.days
      time_sec = 0

    print(' > Propagation from {0} to {1}'.format(t,t+time))
      
    # Propagation of the population
    os.system('sed -i -e "s/.*output_step_day.*/output_step_day = {0}/g" {1}'.format(parameters['output_step_day'],parameters['orbit_propagator']))
    os.system('sed -i -e "s/.*output_step_sec.*/output_step_sec = {0}/g" {1}'.format(parameters['output_step_sec'],parameters['orbit_propagator']))    
    os.system('sed -i -e "s/.*time_day.*/time_day = {0}/g" {1}'.format(time_day,parameters['orbit_propagator']))
    os.system('sed -i -e "s/.*time_sec.*/time_sec = {0}/g" {1}'.format(time_sec,parameters['orbit_propagator']))
    list_ephemeris = propagate(work_dir,parameters['path_op'],population,parameters['orbit_propagator'])
    snapshots = convert_ephemeris_2_snapshot(list_ephemeris)

    print('')
    print(' > Conversion snapshots')
    
    # Save the population
    for snapshot in snapshots:
      date = snapshot[0]
      name = 'population_'+date.strftime('%Y%m%d')
      snapshot[1].to_csv(pop_dir+'/'+name+'.csv',index=False)
      t = date
      population = snapshot[1][parameters['headers']]
      
      print('Date {0} with {1} RSOs'.format(t,len(population)))
     
      
  return population


def merge_population(headers,population,fragments):
  """ Merge the population with the new fragments.

  INPUTS
  ------

  headers: array of string
    Headers of the population.
  population: dataframe
    Initial population of RSOs.
  fragments: dataframe
    Fragments created.

  RETURN
  ------

  population: dataframe
    New populations of RSOs.

  """

  id_max = population['Id Norad'].max()
  fragments['Id Norad'] = id_max
  for i in range(0,len(fragments),1):
    fragments['Id Norad'] += i
  population = pd.concat([population[headers],fragments[headers]])

  return population

  
    
  
