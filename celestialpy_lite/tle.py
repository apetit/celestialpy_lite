#!/usr/bin/env python3
# Licensed 3-clause BSD style license - see LICENSE.rst
# -*-coding:Utf-8 -*

"""
  tle.py
  Author: Alexis Petit, PhD Student, Namur University
  Date: 2017 02 23
  This script is a part of CelestialPy. It reads the TLE format.
"""


#Packages


import pandas as pd
import numpy as np
from datetime import date,datetime,timedelta
from sgp4.earth_gravity import wgs72
from sgp4.io import twoline2rv


#Internal packages


from celestialpy_lite.constants import earth_eq_radius,earth_mu
from celestialpy_lite.coordinates import cart_2_kepl


#Code


__all__ = [
    'TLE_data',
    'tle_reader'
]

class TLE_data():
  """ A class reading the TLE format.
  """

  def __init__(self,data_tle,format_tle='2_lines',method='raw',date=None):
    """ Read TLE format and compute orbital elements 
    and informations about the RSO.

    INPUTS
    ------

    data_tle: list of string
      Contains one set or more TLE lines.
    format_tle: string
      Give the TLE format (2 or 3 lines).
    method: string
      Give the method to compute orbital elements.
    date: datetime
      Date of the coordinates.

    """
    
    if (format_tle=='2_lines') or (format_tle=='2_lines_serie',method):
      self.name         = 'None'
      self.tle          = data_tle
      self.id_year      = data_tle[0][9:11]
      self.id_launch    = data_tle[0][11:14]
      self.id_piece     = data_tle[0][14:15]
      self.id_norad     = int(data_tle[1][2:7])
      self.observations = self.read(format_tle,method,date)
      
    elif (format_tle=='name_plus_2_lines'):
      self.name         = data_tle[0][2:].strip()
      self.tle          = data_tle[1:]
      self.id_year      = data_tle[1][9:11]
      self.id_launch    = data_tle[1][11:14]
      self.id_piece     = data_tle[1][14:15]
      self.id_norad     = int(data_tle[2][2:7])
      idcospar_part1    = data_tle[0][9:14]
      idcospar_part2    = data_tle[0][14:18].strip()
      self.id_cospar    = idcospar_part1+'-'+idcospar_part2
      format_tle = '2_lines_serie'
      self.observations = self.read(format_tle,method,date)
      
    else:
      print('Error: bad format')

      
  def read(self,format_tle,method,date):
    """ Read the TLE lines and extract the date, the orbital elements
    and the bstar parameter.

    INPUT
    -----

    format_tle: string
      Format of the TLE data.
    method: string
      Method to transform TLE data.
    date: datetime
      Date of the coordinates.

    Return
    ------

    observations: dataframe object
      Contains all observations.

    """

    observations = []

    if format_tle=='2_lines':

        line1 = self.tle[0].strip()
        line2 = self.tle[1].strip()        
        observation = self.convert_one_tle(line1,line2,method,date)
        if observation != None:
          observations.append(observation)
          
    elif format_tle=='2_lines_serie':
      
      for k in range(0,len(self.tle),2):
        line1 = self.tle[k].strip()
        line2 = self.tle[k+1].strip()
        observation = self.convert_one_tle(line1,line2,method,date)
        if observation != None:
          observations.append(observation) 
      
    headers = ["Date","a[m]","e","i[deg]","RAAN[deg]","Omega[deg]","MA[deg]","BSTAR[m2/kg]","BC[m2/kg]"]
    observations = pd.DataFrame(observations,columns=headers)

    return observations
  

  def convert_one_tle(self,line1,line2,method,date_coord):
    """ Convert one pseudo-observation TLE to orbital elements.

    INPUTS
    ------

    line1: string
      First TLE line.
    line2: string 
      Second TLE line.
    method: string
      Method to transform TLE data.
    date_coord: datetime
      Date of the coordinates

    RETURN
    ------

    observation: list
      Contain the date, orbital elements, ballistic coefficients.

    """

    try:

      if method=='raw':
        
        # Compute the date 
        if int(line1[18:20])<50:
          year = 2000 + int(line1[18:20])
        else:
          year = 1900 + int(line1[18:20])
        doy = float(line1[20:32]) - 1
        date = datetime(year,1,1) + timedelta(days=doy)
        
        # Compute the orbital elements
        n = float(line2[52:63])*2*np.pi/86400. #(rad per sec)
        sma = (earth_mu/n**2)**(1./3.)
        ecc = float('0.'+line2[26:33])
        inc = float(line2[8:16])
        raan = float(line2[17:25])
        arg = float(line2[34:42])
        mea = float(line2[43:51])
        
        # Compute the BSTAR parameter
        bstar = float('0.'+line1[54:59])*10**float(line1[59:61])#*100
        bc = 12.741621*bstar

        # Add the data in a list
        observation = [date,sma,ecc,inc,raan,arg,mea,bstar,bc]
        
        return observation

      elif method=='sgp4':

        line1 = self.tle[0].strip()
        line2 = self.tle[1].strip()
        satellite = twoline2rv(line1,line2,wgs72)

        if type(date_coord)==type(None):
          date_coord = satellite.epoch
        else:
          date_tle = satellite.epoch
          if date_coord<date_tle:
            print('Error with the date')
            sys.exit()
            
        yy = int(date_coord.strftime('%Y'))
        mm = int(date_coord.strftime('%m'))
        dd = int(date_coord.strftime('%d'))
        hh = int(date_coord.strftime('%H'))
        mi = int(date_coord.strftime('%M'))
        ss = float(date_coord.strftime('%S.%f'))       
            
        position,velocity = satellite.propagate(yy,mm,dd,hh,mi,ss)
        coord = list(position+velocity)
        coord = [ float(item)*1000. for item in coord ]
        coord = cart_2_kepl(coord,earth_mu)
        sma = coord[0]
        ecc = coord[1]
        inc = coord[2]*180./np.pi
        raan = coord[3]*180./np.pi
        arg = coord[4]*180./np.pi
        mea = coord[5]*180./np.pi

        # Compute the BSTAR parameter
        bstar = float('0.'+line1[54:59])*10**float(line1[59:61])#*100
        bc = 12.741621*bstar
      
        # Add the data in a list
        observation = [date_coord,sma,ecc,inc,raan,arg,mea,bstar,bc]

        return observation
      
      else:

        print('Error with the method to compute orbital elements from TLE data')
        
    except ValueError:

      print('Error with the TLE data')
      
      return None


  def get_idnorad(self):
    """ Give the ID NORAD
    """
    
    return self.id_norad


  def get_idcospar(self):
    """ Give the ID COSPAR
    """
    
    return self.id_cospar
  
  
  def get_name(self):
    """ Give the name of the TLE object
    """
    
    return self.name

  
  def get_id_year(self):
    """ Give the International Designator (Last two digits of launch year) 
    """
    
    return id_year

  def get_id_launch(self):
    """ Give International Designator (Launch number of the year)
    """

    return id_launch
      
   
  def get_id_piece(self):
    """ Give the International Designator (piece of the launch)
    """
    
    return id_piece

  
  def get_observations(self):
    """ Give the observations extracted from the TLE data
    """

    return self.observations
  

  def plot_orbital_evolution(self,save=0):
    """ Plot the orbital elements (a,e,i,RAAN,Omega,ME)
    """

    xticks = [x.strftime('%Y\n%m/%d') for x in self.observations["Date"]]
    #print xticks
    point = 'ro'
    
    fig = plt.figure(figsize=(15,12))
    fig.suptitle('ID NORAD : '+str(self.id_norad),fontsize=22)
    ax1 = fig.add_subplot(3,2,1)
    ax1.grid(True)
    ax1.spines["top"].set_visible(False)
    ax1.spines["right"].set_visible(False)
    ax1.set_ylabel('a [km]',fontsize=14)
    ax1.plot(self.observations["Date"],self.observations["a[m]"]/1000.,point)
    ax1.plot(self.observations["Date"],self.observations["a[m]"]/1000.)
    ax1.set_xticks(ax1.get_xticks()[::3])
    ax2 = fig.add_subplot(3,2,2)
    ax2.grid(True)
    ax2.spines["top"].set_visible(False)
    ax2.spines["right"].set_visible(False)
    ax2.set_ylabel('e',fontsize=14)
    ax2.plot(self.observations["Date"],self.observations["e"],point)
    ax2.plot(self.observations["Date"],self.observations["e"])
    ax2.set_xticks(ax2.get_xticks()[::3])
    ax3 = fig.add_subplot(3,2,3)
    ax3.grid(True)
    ax3.spines["top"].set_visible(False)
    ax3.spines["right"].set_visible(False)
    ax3.set_ylabel('i [deg]',fontsize=14)
    ax3.plot(self.observations["Date"],self.observations["i[deg]"],point)
    ax3.plot(self.observations["Date"],self.observations["i[deg]"])
    ax3.set_xticks(ax3.get_xticks()[::3])
    ax4 = fig.add_subplot(3,2,4)
    ax4.grid(True)
    ax4.spines["top"].set_visible(False)
    ax4.spines["right"].set_visible(False)
    ax4.set_ylabel('$\Omega$ [deg]',fontsize=14)
    ax4.plot(self.observations["Date"],self.observations["RAAN[deg]"],point)
    ax4.plot(self.observations["Date"],self.observations["RAAN[deg]"])
    ax4.set_ylim([0,360])
    ax4.set_xticks(ax4.get_xticks()[::3])
    ax5 = fig.add_subplot(3,2,5)
    ax5.grid(True)
    ax5.spines["top"].set_visible(False)
    ax5.spines["right"].set_visible(False)
    ax5.set_ylabel('$\omega$ [deg]',fontsize=14)
    ax5.set_xlabel('Time',fontsize=14)
    ax5.plot(self.observations["Date"],self.observations["Omega[deg]"],point)
    ax5.plot(self.observations["Date"],self.observations["Omega[deg]"])    
    ax5.set_ylim([0,360])
    ax5.set_xticks(ax5.get_xticks()[::3])
    ax6 = fig.add_subplot(3,2,6)
    ax6.grid(True)
    ax6.spines["top"].set_visible(False)
    ax6.spines["right"].set_visible(False)
    ax6.set_ylabel('M [deg]',fontsize=14)
    ax6.set_xlabel('Time',fontsize=14)
    ax6.plot(self.observations["Date"],self.observations["MA[deg]"],point)
    ax6.plot(self.observations["Date"],self.observations["MA[deg]"])
    ax6.set_ylim([0,360])
    ax6.set_xticks(ax6.get_xticks()[::3])
    if save==0:
      plt.show()
    else:
      plt.savefig(str(self.id_norad)+'_oe')
      

  def plot_bstar(self):
    """ Plot the BSTAR parameter
    """
    
    plt.title('ID NORAD : '+str(self.id_norad),fontsize=14)
    plt.xlabel('Time',fontsize=14)
    plt.ylabel('BC [m2/kg]',fontsize=14)
    plt.plot(self.observations["Date"],self.observations["BSTAR[m2/kg]"])
    plt.show()


def tle_reader(name_file,format_tle):
    """ Convert a file containing a serie of pseudo-observations TLE.

    INPUT
    -----

    name_file: string
      Name of the file.

    RETURN
    ------

    name: string
      Name of the object.
    idn: integer
      The ID NORAD number.
    obs: dataframe
      Contain the date, orbital elements and ballistic coefficients.

    """

    f = open(name_file,'r')
    data = f.readlines()
    f.close()
    tle = TLE_data(data,format_tle)
    obs = tle.get_observation()
    idn = tle.get_idnorad()
    name = tle.get_name()
    
    return name,idn,obs
  

def tle_population_reader(name_file):
    """ Convert a file containing a serie of pseudo-observations TLE.

    INPUT
    -----

    name_file: string
      Name of the file.

    RETURN
    ------

    name: string
      Name of the object.
    idns: integer
      The ID NORAD number.
    obs: dataframe
      Contain the date, orbital elements and ballistic coefficients.

    """

    names = []
    idns = []
    obs = []

    f = open(name_file,'r')
    data = f.readlines()
    f.close()
    for i in range(0,len(data),3):
      tle = TLE_data(data[i:i+3],'3_lines')
      names.append(tle.get_name())      
      idns.append(tle.get_idnorad())
      obs.append(tle.get_observation())
      
    return names,idns,obs
  
    
def last_position(name_file):

    f = open(name_file,'r')
    data = f.readlines()
    f.close()
    tle = TLE_data(data)
    obs = tle.get_observation()
    kepl = np.zeros(6)
    kepl[0]=obs.loc[1,"a[m]"]
    kepl[1]=obs.loc[1,"e"]
    kepl[2]=obs.loc[1,"i[deg]"]
    kepl[3]=obs.loc[1,"RAAN[deg]"]
    kepl[4]=obs.loc[1,"Omega[deg]"]
    kepl[5]=obs.loc[1,"MA[deg]"]

    # Compute the latitude, longitude and altitude from the orbital elements
    cart = coordinates.kepl_2_cart(kepl,earth_mu)
    sph = coordinates.cart_2_sph(cart)
    alt = (sph[0]-earth_eq_radius)/1000.
    print('Altitude  : ',alt)
    print('Latitude  : ',sph[1]*180/np.pi)
    print('Longitude : ',sph[2]*180/np.pi)
    plot_orbit.map_position(alt,sph[1]*180/np.pi*0,sph[2]*180/np.pi*0)    
    
    
def convert_oe_2_tle(format_tle,name,id_norad,date,sma,ecc,inc,raan,per,ma,bc): 
  """ From information about an objects and orbital elements, write a pseudo-observation TLE.

  INPUT
  -----

  name: string
    Name of the object.
  id_norad: integer
    The ID NORAD number.
  date: datetime
    Date of the pseudo-observation.
  sma: float
    Semi-major axis (m).
  ecc: float
    Eccentricity.
  inc: float
    Inclination (deg).
  raan: float
    Right ascension of the ascending node (deg).
  per: float
    Argument of the perigee (deg).
  ma: float
    Mean anomaly (deg).
  bc: float
    Ballistic coefficient (m2/kg).

  RETURN
  ------

  line1: string
    First TLE line.
  line2: string
    Second TLE line.
  line3: string
    Third TLE line.

  """

  #ISS DEB [TOOLBAG]       
  #1 33442U 98067BL  09215.54829407  .13008691  12713-4  15349-3 0  3986
  #2 33442 051.6268 036.9885 0007699 292.6234 072.1768 16.49476607 40751

  int_year = 0
  launch_nb = 1
  piece_of_launch = 'A'
  int_year  = "%02d" % int(int_year)
  launch_nb = "%03d" % int(launch_nb)
  yy = date.strftime('%Y')[-2:]
  doy = "%03d" % int(date.strftime('%j')) 
  dec_day = '%09.8f' % float((int(date.strftime('%H'))*3600.+int(date.strftime('%M'))*60+float(date.strftime('%S')))/86400.)
  id_norad = "%05d"   % int(id_norad)
  inc      = "%08.4f" % inc
  raan     = "%07.4f" % raan
  ecc      = "%08.7f" % ecc
  per      = "%08.4f" % per
  ma       = "%08.4f" % ma
  n = (earth_mu/float(sma)**3)**(1/2.)*86400/np.pi/2
  n = "%010.8f" % n
  if float(bc)>=0:
    sign = '+'
  else:
    sign = '-'

  # Return a TLE data  
  if format_tle=='2_lines':
    line1 = '1 '+str(id_norad)+'U '+str(int_year).zfill(2)+str(launch_nb).zfill(3)+piece_of_launch+'   '+str(yy)+str(doy).zfill(3)+'.'+str(dec_day)[2:].zfill(8)+' '+sign+'.'
    line2 = '2 '+str(id_norad)+' '+str(inc)+' '+str(raan)+' '+str(ecc[2:])+' '+str(per).zfill(3)+' '+str(ma).zfill(3)+' '+str(n).zfill(2)+'    05'
    return line1,line2
  
  elif format_tle=='3_lines':

    line1 = '1 '+str(name)
    line2 = '2 '+str(id_norad)+'U '+str(int_year).zfill(2)+str(launch_nb).zfill(3)+piece_of_launch+'   '+str(yy)+str(doy).zfill(3)+'.'+str(dec_day)[2:].zfill(8)+' '+sign+'.'
    line3 = '3 '+str(id_norad)+' '+str(inc)+' '+str(raan)+' '+str(ecc[2:])+' '+str(per).zfill(3)+' '+str(ma).zfill(3)+' '+str(n).zfill(2)+'    05'

    return line1,line2,line3


def write_orbital_elements(observations,name_file):
  """ Write date, orbitals elements and ballistic coefficients.
  """

  pass



  

  
 
