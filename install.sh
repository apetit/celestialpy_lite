#!/bin/bash -i
#Author: Alexis Petit, IMCCE, Observatoire de Paris
#Date: 2019/12/01
#This script install the environment for the Python package CelestialPy.

echo 'Installation of the Python package CelestialPy Lite V2.0'

#Create the conda environment
conda create -n celestialpy_lite python=3
conda install -n celestialpy_lite pip numpy scipy pandas jdcal 
conda install -n celestialpy_lite -c conda-forge matplotlib=3.1.1 sgp4
conda activate celestialpy_lite
pip install cython
pip install progressbar
conda deactivate celestialpy_lite
echo ''
echo 'Installation done!'
echo ''
echo 'To activate the environment: conda activate celestialpy_lite'
echo ''
echo 'Warning: you need to configurate the user file and the variable of environment.'
echo 'To obtain the list of the environments: conda env list'
echo 'To remove: conda remove --name celestialpy_lite --all'

