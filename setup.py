#!/usr/bin/python
# Licensed under a 3-clause BSD style license - see LICENSE.rst
# -*-coding:Utf-8 -*

from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='celestialpy_lite',
    version='1.0.0',
    description='Package to deal with space flight and space debris problems',
    long_description=readme,
    author='Alexis Petit',
    author_email='alexis.petit@obspm.fr',
    url='https://gitlab.obspm.fr/apetit/celestialpy_lite',
    license=license,
    packages=find_packages(exclude=('scripts'))
)

